<div align="center">

[![pipeline status](https://gitlab.com/MarJose/evotingsystem/badges/master/pipeline.svg)](https://gitlab.com/MarJose/evotingsystem/commits/master)
[![GPLv3 license](https://img.shields.io/badge/license-GPLv3-blue.svg?style=flat-square)](https://gitlab.com/MarJose/evotingsystem/blob/master/LICENSES.MD)
[![](https://img.shields.io/badge/laravel-v6.x-red.svg?style=flat-square&logo=laravel&logoColor=red)](https://www.laravel.com/)
![](https://img.shields.io/badge/completed-70%25-green.svg?style=flat-square)
![](https://img.shields.io/badge/status-ongoing-blue.svg?style=flat-square)
[![Donate](https://img.shields.io/badge/donate-PayPal-green.svg?style=flat-square&logo=paypal)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YW5YQXDMQTCUJ&source=url) 

</div>




## System Requirements
To be able to run Laravel Boilerplate you have to meet the following requirements:
- PHP > 7.2
- PHP Extensions:  Mbstring, Tokenizer, Mcrypt, BCMath, Ctype, JSON, OpenSSL, XML
- Node.js > 6.0
- Composer >= 1.9.0


## Installation
1. Install Composer using detailed installation instructions [here](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
```
$ wget https://getcomposer.org/composer.phar
$ chmod +x composer.phar
$ mv composer.phar /usr/local/bin/composer
```
or 
```bash
sudo apt update -y && sudo apt install composer -y
```
or
2. Install Node.js using detailed installation instructions [here](https://nodejs.org/en/download/package-manager/)
```
$ sudo apt install npm
```
3. Clone repository
```
$ git clone https://gitlab.com/MarJose/evotingsystem.git
```
4. Change into the working directory
```
$ cd evotingsystem
```
5. Copy `.env.example` to `.env` and modify according to your environment
```
$ cp .env.example .env
```
6. Install composer dependencies
```
$ composer install --prefer-dist
```
7. Migrate Tables with the command
```
$ php artisan migrate 
```
8. An application key can be generated with the command
```
$ php artisan key:generate
```
## This Project Composed of:
- [Laravel Excel](https://github.com/maatwebsite/Laravel-Excel) - MIT

- [Laravel](https://github.com/laravel/laravel) - MIT

- [Laravel Framework](https://github.com/laravel/framework) - MIT

- [Gentelella (Colorlib)](https://github.com/ColorlibHQ/gentelella) - MIT

- [SweetAlert2](https://github.com/sweetalert2/sweetalert2) - MIT

- [Semantic UI](https://github.com/Semantic-Org/Semantic-UI) - MIT

- [Laravel Websockets](https://beyondco.de/docs/laravel-websockets) - MIT

---

## License
- [GNU GPL3](https://choosealicense.com/licenses/gpl-3.0/)

Permissions of this strong copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. 


## Donate ♥♥
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YW5YQXDMQTCUJ&source=url)
[![](https://img.shields.io/badge/Donate-bitcoin-red.svg?style=flat-square&logo=bitcoin&logoColor=red)](https://www.blockchain.com/)
