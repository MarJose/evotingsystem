/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/*******************************
        Define Sub-Tasks
*******************************/

module.exports = function(gulp) {

  var
    // rtl
    buildRTL     = require('./../rtl/build'),
    watchRTL     = require('./../rtl/watch')
  ;

  gulp.task('watch-rtl', 'Build all files as RTL', watchRTL);
  gulp.task('build-rtl', 'Watch files as RTL ', buildRTL);

};
