/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/*
 Used to import GitHub Auth Token
 To Automate GitHub Updates
*/

module.exports = {
  token    : 'AN-OAUTH2-TOKEN',
  username : 'github-username',
  name     : 'Your Name',
  email    : 'user@email.com'
};
