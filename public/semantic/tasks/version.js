/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/*******************************
          Version Task
*******************************/

var
  release = require('./config/project/release')
;

module.exports = function(callback) {
  console.log(release.title + ' ' + release.version);
};
