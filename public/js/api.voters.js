/*
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/*
*  SETUP API END POINT
* */
$.fn.api.settings.api = {
    'upVote' : route('voters.voters.vote'),
    'downVote'   : 'vote/{position}/{id}',
};

$.fn.api.settings.successTest = function(response) {
    if(response && response.success) {
        return response.success;
    }
    return false;
};



$('.ui.inverted.green.button').api({
        action: 'upVote',
        method: 'POST',
        data: {
            position: $(this).data("position"),
            id : $(this).data("id")
        },
        // modify data PER element in callback
        beforeSend: function(settings) {
            // cancel request if no id
            if(!$(this).data('id') || !$(this).data('position')) {
                return false;
            }
            settings.data.id = $(this).data('id');
            settings.data.position = $(this).data('position');
            return settings;
        },
        beforeXHR: function(xhr) {
            // adjust XHR with additional headers
            xhr.setRequestHeader ('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            return xhr;
        },
        onResponse: function(response) {
            // make some adjustments to response
            return response;
        },
        successTest: function(response) {
            // test whether a JSON response is valid
            console.log(response);
            return response.success || false;

        },
        onComplete: function(response) {
            // always called after XHR complete
            console.log(response);
        },
        onSuccess: function(response) {
            // valid response and response.success = true
            console.log(response);
        },
        onFailure: function(response) {
            // request failed, or valid response but response.success = false
            console.log("Failure: ".concat(response));
        },
        onError: function(errorMessage) {
            // invalid response
            console.log("Error: " + errorMessage);
        },
        onAbort: function(errorMessage) {
            // navigated to a new page, CORS issue, or user canceled request
            console.log("Abort: ".concat(errorMessage));
        }

    }).state({
        onActivate: function() {
            $(this).state('flash text');
        },
        text: {
            inactive   : '<i class="thumbs up outline icon"></i> Vote',
            active     : '<i class="heart icon red"></i> Voted',
            deactivate : '<i class="thumbs down outline icon"></i> Remove Vote',
            flash      : 'Successfully Voted!'
        }
    });

