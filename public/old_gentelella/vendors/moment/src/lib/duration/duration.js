/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

// Side effect imports
import './prototype';

import { createDuration } from './create';
import { isDuration } from './constructor';
import { getSetRelativeTimeThreshold } from './humanize';

export {
    createDuration,
    isDuration,
    getSetRelativeTimeThreshold
};
