/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

export default function isObject(input) {
    return Object.prototype.toString.call(input) === '[object Object]';
}
