/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

export var defaultInvalidDate = 'Invalid date';

export function invalidDate () {
    return this._invalidDate;
}
