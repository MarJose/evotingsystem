/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

module.exports = function(grunt) {
   grunt.registerTask('assets:production',
   [
      'cssmin:plugin',
      'uglify:plugin'
   ]);
};
