/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

module.exports = {
   options: {
      shorthandCompacting: false
   },
   plugin: {
      files: {
         'dist/jquery.tagsinput.min.css': ['src/jquery.tagsinput.css']
      }
   }
};
