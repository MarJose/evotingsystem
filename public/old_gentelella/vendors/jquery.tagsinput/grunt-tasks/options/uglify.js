/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

module.exports = {
   options: {
      mangle: true
   },
   plugin: {
      files: {
         'dist/jquery.tagsinput.min.js': ['src/jquery.tagsinput.js']
      }
   }
};
