/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define(["./raphael.core", "./raphael.svg", "./raphael.vml"], function(R) {

    return R;

});
