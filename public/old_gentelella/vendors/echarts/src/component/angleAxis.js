/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define(function(require) {
    'use strict';

    require('../coord/polar/polarCreator');

    require('./axis/AngleAxisView');
});
