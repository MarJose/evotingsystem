/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/**
 * @file Timeline view
 */
define(function (require) {

    // var zrUtil = require('zrender/core/util');
    // var graphic = require('../../util/graphic');
    var ComponentView = require('../../view/Component');

    return ComponentView.extend({

        type: 'timeline'
    });

});
