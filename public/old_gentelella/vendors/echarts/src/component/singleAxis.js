/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define(function (require) {

    require('../coord/single/singleCreator');
    require('./axis/SingleAxisView');
    require('../coord/single/AxisModel');

});
