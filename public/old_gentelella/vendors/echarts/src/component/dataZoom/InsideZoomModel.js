/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/**
 * @file Data zoom model
 */
define(function(require) {

    return require('./DataZoomModel').extend({

        type: 'dataZoom.inside',

        /**
         * @protected
         */
        defaultOption: {
            zoomLock: false // Whether disable zoom but only pan.
        }
    });
});
