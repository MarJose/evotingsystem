/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/**
 * visualMap component entry
 */
define(function (require) {

    require('./visualMapContinuous');
    require('./visualMapPiecewise');

});
