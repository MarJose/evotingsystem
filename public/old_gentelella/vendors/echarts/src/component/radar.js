/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define(function (require) {

    require('../coord/radar/Radar');
    require('../coord/radar/RadarModel');

    require('./radar/RadarView');
});
