/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define(function (require) {
    var echarts = require('echarts');
    echarts.dataTool = {
        version: '1.0.0',
        gexf: require('./gexf'),
        prepareBoxplotData: require('./prepareBoxplotData')
    };
    return echarts.dataTool;
});
