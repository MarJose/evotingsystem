/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */
define(function (require) {

    /**
     * @see <https://github.com/mbostock/d3/blob/master/src/arrays/quantile.js>
     * @see <http://en.wikipedia.org/wiki/Quantile>
     * @param {Array.<number>} ascArr
     */
    return function(ascArr, p) {
        var H = (ascArr.length - 1) * p + 1,
            h = Math.floor(H),
            v = +ascArr[h - 1],
            e = H - h;
        return e ? v + e * (ascArr[h] - v) : v;
    };

});
