
/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

// Not working? Did you `npm install` `npm run build` first?

var $ = require("jquery");
var PNotify = require("pnotify");

$(function(){
    $("#button1").click(function(){
        new PNotify({
            title: "Yay!",
            text: "It works!"
        });
    });

    $("#button12").click(function(){
        require("pnotify.reference");

        new PNotify({
            title: "Yay!",
            text: "It works!",
            reference: {
                put_thing: true
            }
        });
    });
});
