/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define( function() {

/**
 * Determines whether an object can have data
 */
return function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};

} );
