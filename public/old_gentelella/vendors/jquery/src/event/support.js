/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define( [
	"../var/support"
], function( support ) {

support.focusin = "onfocusin" in window;

return support;

} );
