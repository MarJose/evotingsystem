/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

/*global Switchery:true*/ // Meteor creates a file-scope global for exporting. This comment prevents a potential JSHint warning.
Switchery = window.Switchery;
delete window.Switchery;
