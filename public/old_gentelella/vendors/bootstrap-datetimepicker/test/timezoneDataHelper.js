/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

(function () {
    'use strict';
    $.ajax('node_modules/moment-timezone/data/packed/latest.json', {
        success: function (data) {
            moment.tz.load(data);
        },
        method: 'GET',
        dataType: 'json',
        async: false
    });
}());
