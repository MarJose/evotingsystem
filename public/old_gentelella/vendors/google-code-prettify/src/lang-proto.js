/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */


/**
 * @fileoverview
 * Registers a language handler for Protocol Buffers as described at
 * http://code.google.com/p/protobuf/.
 *
 * Based on the lexical grammar at
 * http://research.microsoft.com/fsharp/manual/spec2.aspx#_Toc202383715
 *
 * @author mikesamuel@gmail.com
 */

PR['registerLangHandler'](PR['sourceDecorator']({
        'keywords': (
            'bytes,default,double,enum,extend,extensions,false,'
            + 'group,import,max,message,option,'
            + 'optional,package,repeated,required,returns,rpc,service,'
            + 'syntax,to,true'),
        'types': /^(bool|(double|s?fixed|[su]?int)(32|64)|float|string)\b/,
        'cStyleComments': true
      }), ['proto']);
