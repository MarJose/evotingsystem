/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

// Validation errors messages for Parsley
import Parsley from '../parsley';

Parsley.addMessages('zh-cn', {
  dateiso: "请输入正确格式的日期 (YYYY-MM-DD)."
});
