/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

define([
	"./core",
	"./attributes/attr",
	"./attributes/prop",
	"./attributes/classes",
	"./attributes/val"
], function( jQuery ) {

// Return jQuery for attributes-only inclusion
return jQuery;
});
