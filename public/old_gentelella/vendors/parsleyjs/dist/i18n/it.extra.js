/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('it', {
  dateiso: "Inserire una data valida (AAAA-MM-GG)."
});
