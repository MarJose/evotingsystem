/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('fr', {
  dateiso: "Cette valeur n'est pas une date valide (YYYY-MM-DD).",
  notequalto: "Cette valeur doit être différente."
});
