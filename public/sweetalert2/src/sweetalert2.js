/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

import SweetAlert from './SweetAlert.js'

const Swal = SweetAlert
Swal.default = Swal

export default Swal
