/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

import * as dom from '../../dom/index.js'

export const renderTitle = (instance, params) => {
  const title = dom.getTitle()

  dom.toggle(title, params.title || params.titleText)

  if (params.title) {
    dom.parseHtmlToContainer(params.title, title)
  }

  if (params.titleText) {
    title.innerText = params.titleText
  }

  // Custom class
  dom.applyCustomClass(title, params.customClass, 'title')
}
