/*
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 */

// Detect Node env
export const isNodeEnv = () => typeof window === 'undefined' || typeof document === 'undefined'
