<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019. 
 *
 * @Licenses GNU GPLv3 
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->binary('avatar')->nullable();
            $table->string('Student_ID')->unique();
            $table->string('password')->nullable();
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('MiddleName');
            $table->integer('GradeLvl');
            $table->boolean('doneVoting')->default(0);
            $table->boolean('isActive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
