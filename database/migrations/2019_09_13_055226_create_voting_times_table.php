<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019. 
 *
 * @Licenses GNU GPLv3 
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotingTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_start')->nullable();
            $table->time('time_start')->nullable();
            $table->date('date_end')->nullable();
            $table->time('time_end')->nullable();
            $table->boolean('isopen')->default('0');
            $table->boolean('isOpenLiveMonitor')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_times');
    }
}
