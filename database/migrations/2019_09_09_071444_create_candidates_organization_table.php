<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019. 
 *
 * @Licenses GNU GPLv3 
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates_organization', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('candidates_id')->unsigned();
            $table->integer('organization_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_organization');
    }
}
