<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeatsAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seats_availabilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('president')->default(1);
            $table->integer('vice_president')->default(1);
            $table->integer('secretary')->default(1);
            $table->integer('treasurer')->default(1);
            $table->integer('auditor')->default(2);
            $table->integer('pio')->default(2);
            $table->integer('business_manager')->default(1);
            $table->integer('senior_representative')->default(1);
            $table->integer('junior_representative')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seats_availabilities');
    }
}


