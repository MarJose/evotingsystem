<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019. 
 *
 * @Licenses GNU GPLv3 
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use App\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        Student::truncate();


        Student::create([
            'Student_ID' => '1234',
            'avatar' => '/img/avatar.png',
            'password' => Hash::make('1234'),
            'FirstName' => 'MarJose',
            'LastName' => 'Darang',
            'MiddleName' => 'O',
            'GradeLvl' => '12'

        ]);
    }
}
