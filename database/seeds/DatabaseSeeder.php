<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019. 
 *
 * @Licenses GNU GPLv3 
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            SeatsAvailabilitySeeder::class,
            RolesTableSeeder::class,
            UserTableSeeder::class,
            StudentTableSeeder::class,
            VotingTimeTableSeeder::class,
        ]);
    }
}
