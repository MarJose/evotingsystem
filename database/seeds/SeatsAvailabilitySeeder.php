<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use App\SeatsAvailability;
use Illuminate\Database\Seeder;

class SeatsAvailabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SeatsAvailability::truncate();

        $seats = new SeatsAvailability();
        $seats->president = 1;
        $seats->vice_president = 1;
        $seats->secretary = 1;
        $seats->treasurer = 1;
        $seats->auditor = 2;
        $seats->pio = 2;
        $seats->business_manager = 1;
        $seats->senior_representative = 1;
        $seats->junior_representative = 1;
        $seats->saveOrFail();

    }
}

