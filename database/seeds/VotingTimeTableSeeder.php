<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019. 
 *
 * @Licenses GNU GPLv3 
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use App\VotingTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class VotingTimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        VotingTime::truncate();

        VotingTime::create([
            'date_start' => now()->addDays(16)->toDateString(),
            'time_start' => '10:29:00',
            'isopen' => 1,
        ]);

    }
}
