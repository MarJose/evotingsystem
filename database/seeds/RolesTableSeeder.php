<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // truncate the table
        DB::table('permissions')->delete();
        DB::table('roles')->delete();
        DB::table('role_has_permissions')->delete();
        DB::table('model_has_roles')->delete();
        DB::table('model_has_permissions')->delete();

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        /*
         * permission per module
         *
         * */


        // Voters Section
        Permission::create(['guard_name' => 'web', 'name' => 'voters section']); // module
        Permission::create(['guard_name' => 'web', 'name' => 'import voters']);
        Permission::create(['guard_name' => 'web', 'name' => 'view voters']); // list view
        Permission::create(['guard_name' => 'web', 'name' => 'add voters']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete voters']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit voters']);

        // candidate Section
        Permission::create(['guard_name' => 'web', 'name' => 'candidate section']); // module
        Permission::create(['guard_name' => 'web', 'name' => 'view candidate']); // list view
        Permission::create(['guard_name' => 'web', 'name' => 'add candidate']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete candidate']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit candidate']);

        // organization Section
        Permission::create(['guard_name' => 'web', 'name' => 'organization section']);
        Permission::create(['guard_name' => 'web', 'name' => 'view organization']);
        Permission::create(['guard_name' => 'web', 'name' => 'add organization']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete organization']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit organization']);

        // system user
        Permission::create(['guard_name' => 'web', 'name' => 'user section']);
        Permission::create(['guard_name' => 'web', 'name' => 'view users']);
        Permission::create(['guard_name' => 'web', 'name' => 'add user']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete user']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit user']);
        Permission::create(['guard_name' => 'web', 'name' => 'deactivate user']);
        Permission::create(['guard_name' => 'web', 'name' => 'activate user']);

        // system setting
        Permission::create(['guard_name' => 'web', 'name' => 'system section']);
        Permission::create(['guard_name' => 'web', 'name' => 'configure precinct door']);
        Permission::create(['guard_name' => 'web', 'name' => 'configure live monitor']);
        Permission::create(['guard_name' => 'web', 'name' => 'truncate database']);


        $adminRole = Role::create(['guard_name' => 'web', 'name' => 'Administrator']);
        $adminRole->givePermissionTo(Permission::all());
        $dataEncoder = Role::create(['guard_name' => 'web', 'name' => 'Data Encoder'])
            ->givePermissionTo([
                    'voters section', 'import voters', 'view voters', 'add voters', 'edit voters',
                    'candidate section', 'view candidate', 'add candidate', 'edit candidate',
                    'organization section', 'view organization', 'add organization', 'edit organization'
            ]);

    }
}
