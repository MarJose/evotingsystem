<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();

        $avatar = '/img/user/avatar/default_user_avatar.png';
        $admin = User::create([
            'avatar' => $avatar,
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'FirstName' => 'MarJose',
            'Lastname' => 'Darang',
            'email' => 'marjosedarang@gmail.com',
        ]);
        $encoder = User::create([
            'avatar' => $avatar,
            'username' => 'encoder',
            'password' => Hash::make('encoder'),
            'FirstName' => 'encoder',
            'Lastname' => 'encoder',
            'email' => 'encoder@gmail.com',
        ]);

        $admin->assignRole('Administrator');
        $encoder->assignRole('Data Encoder');




    }
}
