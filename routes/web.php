<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Events\ServerMonitor;
use App\Notifications\DoneVoting;
use App\Student;
use App\Traits\SystemInfo;
use App\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;

Route::redirect('/','/vote/login');

/*
 *  For System Administrator Auth Path
 */
Route::get('/sysadmin/', 'sysadmin\auth\login@index')->name('sysadmin.login.page');
Route::post('/sysadmin/login','sysadmin\auth\login@logmein')->name('sysadmin.login');
Route::get('/sysadmin/login/success','sysadmin\auth\login@successlogin');
Route::post('/sysadmin/logout','sysadmin\auth\login@logout')->name('sysadmin.logout');

Route::get('/test', function () {
    $user = User::all();
    $student = Student::all();
    //ddd($student[0]);
    Notification::send($user, new DoneVoting($student[0]));
    return 'Event Fired';
});


/*
 *  sysadmin Dashboard
 */
Route::namespace('sysadmin')->prefix('sysadmin')->name('sysadmin.')->middleware(['auth'])->group(function () {
    // System Admin and Developer Admin Dashboard
    Route::get('dashboard', 'Dashboard@index')->name('dashboards');
    // Voters / Student Register & info
    Route::resource('voters', 'voters\voters');
    // Generate Random Password
    Route::get('voters/{id}/generate/random/password', 'voters\voters@GeneratePassword')->name('generateRndPassword');
    // Candidates
    Route::resource('candidates', 'candidates\CandidatesController');
    //users
    Route::resource('account/user', 'account\AccountUserController');
    // Profile
    Route::resource('account/profile', 'profile\Profile');
    //user Activate & Deactivate
    Route::post('account/user/{id}/deactivate', 'account\AccountUserController@Deactivate')->name('user.deactivate');
    Route::post('account/user/{id}/activate', 'account\AccountUserController@Activate')->name('user.activate');
    // Organization
    Route::resource('organization', 'candidates\Organization');
    // General Settings
    Route::resource('settings/general','settings\General');
    // Seats
    Route::get('settings/seats/configuration','settings\SeatsController@index')->name('seats.index');
    Route::put('settings/seats/configuration','settings\SeatsController@update')->name('seats.update');
    // Database Setting
    Route::get('settings/database', 'settings\Database@index')->name('database.page');
    Route::get('settings/database/table/{table}/truncate', 'settings\Database@truncate')->name('database.truncate');
    // Bulk Import Path
    Route::post('voters/bulk/import','voters\voters@import')->name('VotersImport');
    Route::get('voters/bulk/import/file/format','voters\voters@downloadfile')->name('VotersFileFormatImport');

   /*
    *  AJAX Path URL REQUEST
    *
    */
    // system information path
    Route::get('/server/info','sysinfo\info@sysinfo')->name('sysinfopath');

});



/*
 *  For Student Voting Auth Path
 */
Route::get('/vote/login', 'voters\auth\login@index')->name('voters.login.page');
Route::post('/vote/login','voters\auth\login@checklogin')->name('voters.login');
Route::get('/vote/login/success', 'voters\auth\login@successlogin');
Route::post('/vote/logout','voters\auth\login@logout')->name('voters.logout');

/*
 *  Route for student voting dashboard
 * This need to have a auth
 */

Route::namespace('voters')->prefix('precinct')->name('voters.')->middleware(['precinct.door','precinct.voters'])->group(function (){
    Route::get('vote/candidates','vote\CandidatesController@index')->name('dashboards');


    // Semantic API
    Route::post('vote/precinct/candidates', 'vote\VoteController@upVote')->name('voters.vote');

});

Route::view('/vote/close/countdown','partials.vote-is-closed-countdown')->name('countdown.page')->middleware(['precinct.door']);



