<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Imports;

use App\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use OwenIt\Auditing\Contracts\Auditable;

class VotersImport implements ToModel, WithHeadingRow, Auditable
{
    use \OwenIt\Auditing\Auditable;

    public function model(array $row)
    {
        return new Student([
            //
            'Student_ID' => $row[0],
            'FirstName' => $row[1],
            'LastName' => $row[2],
            'MiddleName' => $row[3],
            'GradeLvl' => $row[4],
        ]);
    }
    public function headingRow(): int
    {
        return 1;
    }
}
