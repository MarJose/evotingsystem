<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidates extends Model
{
    //

    protected $fillable = [
        'StudentPicture', 'StudentID','FName' ,'LName', 'Position', 'Gradelvl',
    ];


    public function organizations(){
       return $this->belongsToMany(Organization::class);
    }

    public function hasOrg($org){
        return null !== $this->organizations()->where('Organization_name', $org)->first();
    }
}
