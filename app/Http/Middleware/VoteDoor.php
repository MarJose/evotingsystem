<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Middleware;

use App\Student;
use App\VotingTime;
use Closure;
use Illuminate\Support\Facades\Auth;

class VoteDoor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $votetime = VotingTime::findOrFail(1);
        $isopen = $votetime->isopen;
        $datestart = $votetime->date_start;
        $timestart = $votetime->time_start;


        $currentDate = date('Y-m-d');
        $currentTime = date('H:i:s');
        if($isopen === 1 || $isopen === '1'){
            // no countdown
            //delete session if existing
            if(session()->exists('datestart') && session()->exists('timestart')){
                session()->forget(['datestart','timestart']);
            }

            return $next($request);
        }
        //view countdown
        //update database isActive
        $id = Auth::guard('student')->id();
        $acct = Student::find($id);
        $acct->isActive = 0;
        $acct->save();

        //added the date and time start in session

        session()->put('datestart', $datestart);
        session()->put('timestart', $timestart);
       return redirect()->route('countdown.page');
    }
}
