<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\voters\vote;

use App\Candidates;
use App\Http\Controllers\Controller;

class CandidatesController extends Controller
{
    public function index()
    {
        //
        $president = Candidates::with('organizations')->where('Position', 'president')->get();
        $vpresident = Candidates::where('Position', 'vice-president')->get();
        $secretary = Candidates::where('Position', 'secretary')->get();
        $treasurer = Candidates::where('Position', 'treasurer')->get();
        $auditor = Candidates::where('Position', 'auditor')->get();
        $pio = Candidates::where('Position', 'pio')->get();
        $business_manager = Candidates::where('Position', 'business-manager')->get();
        $senior_representative = Candidates::where('Position', 'senior-representative')->get();
        $junior_representative = Candidates::where('Position', 'junior-representative')->get();


        return view('voters.vote-dashboard')
           /* ->with([
                'presidents' => $president,
                'vpresidents' => $vpresident,
                'secretarys' => $secretary,
                'treasurers' => $treasurer,
                'auditors' => $auditor,
                'pios' => $pio,
                'business_managers' => $business_manager,
                'seniors' => $senior_representative,
                'juniors' => $junior_representative
            ]);*/
           ->with(
               'presidents', $president
           );


    }
}