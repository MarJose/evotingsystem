<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\voters\vote;

use App\AuditorTempVote;
use App\BmTempVote;
use App\Http\Controllers\Controller;
use App\JuniorRepTempVote;
use App\PioTempVote;
use App\PresTempVote;
use App\SeatsAvailability;
use App\SecretaryTempVote;
use App\SeniorRepTempVote;
use App\TreasurerTempVote;
use App\VicePresTempVote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Object_;

class VoteController extends Controller
{

    private $numberOfPosition;
    private $Designation;

    public function upVote(Request $request)
    {
        if($request->ajax())
        {

            /* Filter if the Parameters is present or not */
            if($request->has('position') && $request->has('id')){
                /* Get the Seats Availability for the Position */
                $seatsAvailable = SeatsAvailability::find(1);
                $presidentSeats = $seatsAvailable->president;
                $vicePresidentSeats = $seatsAvailable->vice_president;
                $secretarySeats = $seatsAvailable->secretary;
                $treasurerSeats = $seatsAvailable->treasurer;
                $auditorSeats = $seatsAvailable->auditor;
                $pioSeats = $seatsAvailable->pio;
                $businessManagerSeats = $seatsAvailable->business_manager;
                $seniorRepSeats = $seatsAvailable->senior_representative;
                $juniorRepSeats = $seatsAvailable->junior_representative;

                /* Check first the Position number in the SeatsAvailability*/

                switch ($request->input('position'))
                {
                    case 'president':
                        $this->numberOfPosition = $presidentSeats;
                        break;
                    case 'vice-president':
                        $this->numberOfPosition = $vicePresidentSeats;
                        break;
                    case 'secretary':
                        $this->numberOfPosition = $secretarySeats;
                        break;
                    case 'treasurer':
                        $this->numberOfPosition = $treasurerSeats;
                        break;
                    case 'auditor':
                        $this->numberOfPosition = $auditorSeats;
                        break;
                    case 'pio':
                        $this->numberOfPosition = $pioSeats;
                        break;
                    case 'business-manager':
                        $this->numberOfPosition = $businessManagerSeats;
                        break;
                    case 'senior-representative':
                        $this->numberOfPosition = $seniorRepSeats;
                        break;
                    case 'junior-representative':
                        $this->numberOfPosition = $juniorRepSeats;
                        break;
                    default:
                        $this->numberOfPosition = null;
                        break;
                }


                if(!empty($this->numberOfPosition) || $this->numberOfPosition !== null)
                {
                    /* Check the Number of Position Versus the current vote for the designation */
                    $tempvote = $this->TempVoteCount($request->input('position'));

                    if($tempvote === null)
                    {
                        $response =[
                            'success' => false,
                            'message' => "Unknown error: Unable to get the temporary votes",
                            'data' => [
                                'msg' => 'Please report this issue to your system administrator. Please try refresh page.',
                                'vote-remaining' => 0,
                                'position' => $request->input('position')
                            ]
                        ];
                        return response()->json($response, 200);
                    }


                    if($tempvote === $this->numberOfPosition)
                    {
                        /* Vote Completed for this Position */
                        $response =[
                            'success' => false,
                            'message' => "Vote completed for this position",
                            'data' => [
                                'msg' => 'vote completed',
                                'vote-remaining' => 0,
                                'position' => $request->input('position')
                            ]
                        ];
                        return response()->json($response, 200);
                    }

                    /* Need to Vote again */
                    /* Store the vote to the temporary tables */
                    $tempstorevote = $this->StoreTempVote($request);
                    if($tempstorevote)
                    {
                        $voteremaining = $this->numberOfPosition--;
                        if($voteremaining <= 0 || $voteremaining === 0)
                        {
                            $response =[
                                'success' => false,
                                'message' => "Vote completed for this position",
                                'data' => [
                                    'msg' => 'vote completed',
                                    'vote-remaining' => 0,
                                    'position' => $request->input('position')
                                ]
                            ];
                            return response()->json($response, 200);
                        }

                        if( $voteremaining > 0)
                        {
                            $response =[
                                'success' => false,
                                'message' => "Completely voted this candidates",
                                'data' => [
                                    'msg' => 'Completely voted this candidates',
                                    'vote-remaining' => $voteremaining,
                                    'position' => $request->input('position')
                                ]
                            ];
                            return response()->json($response, 200);
                        }

                    }

                }else{
                    // number of position is null
                    /* Possible cause is that the Position name is not match to the switch case option */
                    $response =[
                        'success' => false,
                        'message' => "The request is not valid due to missing parameters",
                        'data' => []
                    ];
                    return response()->json($response, 400);
                }

            }else{
                $response =[
                    'success' => false,
                    'message' => "The request is not valid due to missing parameters",
                    'data' => []
                ];
                return response()->json($response, 400);
            }


//            $response =[
//                'success' => true,
//                'message' => "We've retrieved your data from the server",
//                'data' => [
//                    'request' => $request->input('id')
//                ]
//            ];
//
//            return response()->json($response, 200);
        }
        return abort(404);
    }

    private function StoreTempVote($request)
    {
        ddd($request);
        $createResponse = null;
        switch ($request->position)
        {
            case 'president':
                $pres = new PresTempVote;
                $pres->candidate_id = $request->id;
                $pres->voted_by = auth()->id();
                $createResponse = $pres->save();
                break;
            case 'vice-president':
                $vicePres = new VicePresTempVote;
                $vicePres->candidate_id = $request->id;
                $vicePres->voted_by = auth()->id();
                $createResponse = $vicePres->save();
                break;
            case 'secretary':
                $sec = new SecretaryTempVote;
                $sec->candidate_id = $request->id;
                $sec->voted_by = auth()->id();
                $createResponse = $sec->save();
                break;
            case 'treasurer':
               $treasure = new TreasurerTempVote;
               $treasure->candidate_id = $request->id;
               $treasure->voted_by = auth()->id();
               $createResponse = $treasure->save();
                break;
            case 'auditor':
                $auditor = new AuditorTempVote;
                $auditor->candidate_id = $request->id;
                $auditor->voted_by = auth()->id();
                $createResponse = $auditor->save();
                break;
            case 'pio':
                $pio = new PioTempVote;
                $pio->candidate_id = $request->id;
                $pio->voted_by = auth()->id();
                $createResponse = $pio->save();
                break;
            case 'business-manager':
                $bm = new BmTempVote;
                $bm->candidate_id = $request->id;
                $bm->voted_by = auth()->id();
                $createResponse = $bm->save();
                break;
            case 'senior-representative':
                $senior = new SeniorRepTempVote;
                $senior->candidate_id = $request->id;
                $senior->voted_by = auth()->id();
                $createResponse = $senior->save();
                break;
            case 'junior-representative':
                $junior = new JuniorRepTempVote;
                $junior->candidate_id = $request->id;
                $junior->voted_by = auth()->id();
                $createResponse = $junior->save();
                break;
        }
        Log::debug('Store Temporary Votes data : '. $createResponse);
        return $createResponse;
    }

    private function TempVoteCount($position)
    {
        $tempvotecount = null;
        switch ($position)
        {
            case 'president':
                $tempvotecount = PresTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'vice-president':
                $tempvotecount = VicePresTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'secretary':
                $tempvotecount = SecretaryTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'treasurer':
                $tempvotecount = TreasurerTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'auditor':
                $tempvotecount = AuditorTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'pio':
                $tempvotecount = PioTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'business-manager':
                $tempvotecount = BmTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'senior-representative':
                $tempvotecount = SeniorRepTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;
            case 'junior-representative':
                $tempvotecount = JuniorRepTempVote::where('voted_by', '=', auth()->id())->get()->count();
                break;

        }
        Log::debug('Temporary Votes: '.$tempvotecount);
        return $tempvotecount;

    }



}
