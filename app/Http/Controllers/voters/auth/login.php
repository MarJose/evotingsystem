<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\voters\auth;

use App\Student;
use Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class login extends Controller
{
    //
    use AuthenticatesUsers;

    public function index(){
        //checking if the user is current authenticated
        if(Auth::guard('student')->check()){
            return redirect()->route('voters.dashboards');
        }
        return view('voters.Auth.login');
    }

    public function checklogin(Request $request){
        $this->validate($request, [
            'votersid' => 'required',
            'passcode' => 'required|alphaNum|min:3'
        ]);



        $studentid = $request->input('votersid');
        $pass = $request->input('passcode');


        if(Auth::guard('student')->attempt(['Student_ID' => $studentid, 'password' => $pass])){
            return redirect('/vote/login/success');
        }else{
            session()->flash('error', 'Please check your Student ID / Passcode.');
            return back()
                ->withInput($request->all());


        }


    }

    public function successlogin(){
        if ( Auth::guard('student')->check() ) {
            //update database isActive
            $id = Auth::guard('student')->id();
            $acct = Student::find($id);
            $acct->isActive = 1;
            $acct->save();

            return redirect()->route('voters.dashboards');
        }
        session()->flash('error', 'Please Login first to authenticate.');
        return redirect()->route('voters.login.page');

    }

    public function logout(Request $request){
        //update database isActive
        $id = Auth::guard('student')->id();
        $acct = Student::find($id);
        $acct->isActive = 0;
        $acct->save();

        Auth::guard('student')->logout();
        $request->session()->invalidate();
        return redirect()->route('voters.login.page');

    }



}
