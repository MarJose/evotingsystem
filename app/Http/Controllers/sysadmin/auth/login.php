<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\auth;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class login extends Controller
{
    //
    use ThrottlesLogins;
    protected $maxAttempts = 5;
    protected $decayMinutes = 1;

    public function username()
    {
        return 'username';
    }

    public function index(){
        if(Auth::check()){
            $user = Auth::user();
            session()->flash('alert-success', 'Welcome back! '.$user->Lastname.', '.$user->Firstname);
            return redirect()->route('sysadmin.dashboards');
        }
        return view('AdminPanel.Auth.login');
    }

    public function logmein(Request $request){
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|alphaNum|min:3'
        ]);

        // login throttle
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }


        $username = $request->input('username');
        $password = $request->input('password');
        if(Auth::attempt(['username' => $username, 'password' => $password])){
            $request->session()->regenerate();
            $user = Auth::user();
            if($user->blocked === 1){
                Auth::logout();
                session()->flash('alert-danger', 'Your account has been blocked by the system administrator.');
                return redirect()->route('sysadmin.login.page')
                    ->withInput($request->all());
            }
            return redirect('/sysadmin/login/success');
        }else{
            //
            $this->incrementLoginAttempts($request);
            session()->flash('alert-warning', 'Invalid Username / Password.');
            return redirect()->route('sysadmin.login.page')
                ->withInput($request->all());
        }

    }

    public function successlogin(Request $request){
        if ( Auth::check() ) {
            return redirect()->route('sysadmin.dashboards');
        }
        session()->flash('alert-warning', 'Please Login first to authenticate.');
        return redirect()->route('sysadmin.login.page');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect()->route('sysadmin.login.page');
    }
}
