<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Database extends Controller
{
    //
    public function index()
    {
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current', $tables);

        $tablesCount = array();
        foreach($tables as $table){
            $tablesCount[] = DB::table($table)->count();
        }

        return view('AdminPanel.systemsetting.database')
            ->with('tables', $tables)
            ->with('tablesCount', $tablesCount)
            ;
    }

    public function truncate($table)
    {
        //dd($table);
        DB::table($table)->truncate();

        session()->flash('alert-success', 'Table '.$table.' has been successfully truncated.');
        return redirect()->back();

    }


}
