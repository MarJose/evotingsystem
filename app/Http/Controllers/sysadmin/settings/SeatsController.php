<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\settings;

use App\Http\Controllers\Controller;
use App\SeatsAvailability;
use Illuminate\Http\Request;

class SeatsController extends Controller
{
    public function index()
    {
        //
        return view('AdminPanel.systemsetting.seats')
            ->with('seats', SeatsAvailability::all());
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'president'  => 'required|numeric|max:1|min:1|digits:1',
            'vpresident'  => 'required|numeric|max:1|min:1|digits:1',
            'secretary'  => 'required|numeric|max:1|min:1|digits:1',
            'treasurer'  => 'required|numeric|max:1|min:1|digits:1',
            'auditor'  => 'required|numeric|max:1|min:1|digits:1',
            'pio'  => 'required|numeric|max:1|min:1|digits:1',
            'bm'  => 'required|numeric|max:1|min:1|digits:1',
            'senoirrep'  => 'required|numeric|max:1|min:1|digits:1',
            'juniorrep'  => 'required|numeric|max:1|min:1|digits:1'
        ]);

        $seats = SeatsAvailability::findOrFail($id);
        $seats->president = $request->input('president');
        $seats->vice_president =$request->input('vpresident');
        $seats->secretary =$request->input('secretary');
        $seats->treasurer = $request->input('treasurer');
        $seats->auditor = $request->input('auditor');
        $seats->pio = $request->input('pio');
        $seats->business_manager = $request->input('bm');
        $seats->senior_representative = $request->input('senoirrep');
        $seats->junior_representative = $request->input('juniorrep');
        $seats->save();
    }
}