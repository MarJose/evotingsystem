<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\settings;

use App\VotingTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class General extends Controller
{

    public function edit($id)
    {
        //
        // $id is has no use the $id has a default value on it from database seeder
        $votingTime = VotingTime::find(1);
        return view('AdminPanel.systemsetting.general')
            ->with('votingTime', $votingTime);

    }


    public function update(Request $request, $id)
    {
       // dd($request);
        //
        //this is for the Live Monitor Update
       if($request->has('livemonitorupdate')){
           //check if the switch is toggle or not
           if($request->has('livemonitorswitch')){
               // result value will be "on". update it into 1 else 0
               $setting = VotingTime::find(1);
               $setting->isOpenLiveMonitor = 1;
               $setting->save();
           }else{
               $setting = VotingTime::find(1);
               $setting->isOpenLiveMonitor = 0;
               $setting->save();
           }
       }


       // this is for the Precinct Door
       if($request->has('precinctdoorupdate')){
            //check if the switch is toggle or not
           if($request->has('precinctDoorSwitch')){
               // result value will be "on". update it into 1 else 0
               // if toggle then time and date field is required
               // do validation for the time and date field
               $this->validate($request, [
                   'precinctDoorDate' => 'required|date_format:Y/m/d',
                    'precinctDoorTime' => 'required'
                   ]);

               // mass update
               $setting = VotingTime::find(1);
               $setting->update([
                   'isopen' => 1,
                   'date_start' => $request->input('precinctDoorDate'),
                   'time_start' => $request->input('precinctDoorTime')
               ]);
           }else{
               $setting = VotingTime::find(1);
               $setting->isopen = 0;
               $setting->save();
           }
       }
        session()->flash('alert-success', 'New Configuration, Successfully Updated!');
        return redirect()->back();
    }


}
