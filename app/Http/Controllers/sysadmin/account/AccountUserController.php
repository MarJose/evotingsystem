<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\account;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AccountUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AdminPanel.users.users')
            ->with('users', User::where('id', '!=', auth()->id())->orderBy('created_at','desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::all();
        return view('AdminPanel.users.add-user')
            ->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
           'userImage' => 'required|mimes:jpg,jpeg,png,gif|max:2048',
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'username' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required|required_with:password',
            'email' =>  'required|email',
            'accountrole' => 'required'
        ]);

        $useravatar = $request->file('userImage');
        $extension = $useravatar->getClientOriginalExtension();
        $path = 'img/user/avatar/'.Str::slug($useravatar->getFilename().Str::random(12).Carbon::now()->timestamp).'.'.$extension;
        Storage::disk('public')->put($path,  File::get($useravatar));

        //
       $user = User::create([
            'avatar' => $path,
            'Firstname' => $request->input('First_Name'),
            'Lastname' =>  $request->input('Last_Name'),
            'username' =>  $request->input('username'),
            'password' =>  bcrypt($request->input('password')),
            'email' =>     $request->input('email')
        ]);

        // attach the role selected
       $user->roles()->attach($request->input('accountrole'));

       // show session flash with message prompt
        session()->flash('alert-success', 'New Account System User has been added successfully!');
        return redirect()->route('sysadmin.user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if(Auth::id() === $id){
            session()->flash('alert-error', 'You cannot edit your Information here!, Please goto user profile and edit there.');
            return back();
        }
        // show the form
        $user = User::find($id);
        $roles = Role::all();
        return view('AdminPanel.users.edit-user')
            ->with('user', $user)
            ->with('roles', $roles);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        //
        if(Auth::id() === $id){
            session()->flash('alert-error', 'You cannot edit/update your Information here!, Please goto user profile and edit there.');
            return back();
        }
        // Update process here!

        $this->validate($request, [
                'First_Name' => 'required',
                'Last_Name' => 'required',
                'username' => 'required|unique:users,username,'.$id,
                'email' => 'required|unique:users,email,'.$id
        ]);

        if($request->has('userImage')){
            // user avatar is present save it also

            $user = User::find($id);

            //delete the previous avatar stored in the storage
            $path = $user->avatar;
            Storage::disk('public')->delete($path);
            // updating the user data
            $useravatar = $request->file('userImage');
            $extension = $useravatar->getClientOriginalExtension();
            $path = 'img/user/avatar/'.Str::slug($useravatar->getFilename().Str::random(12).Carbon::now()->timestamp).'.'.$extension;
            Storage::disk('public')->put($path,  File::get($useravatar));

            //update the model
            $user->update([
                'avatar' => $path,
                'Firstname' => $request->input('First_Name'),
                'Lastname' => $request->input('Last_Name'),
                'username' => $request->input('username'),
                'email' => $request->input('email')
            ]);
            // update the roles
            $user->roles()->sync($request->input('accountrole'));

            session()->flash('alert-success','System Account user was successfully updated.');
            return redirect()->route('sysadmin.user.index');
        }
        else{
            $user = User::find($id);
            //update the model
            $user->update([
                'Firstname' => $request->input('First_Name'),
                'Lastname' => $request->input('Last_Name'),
                'username' => $request->input('username'),
                'email' => $request->input('email')
            ]);
            // update the roles
            $user->roles()->sync($request->input('accountrole'));

            session()->flash('alert-success','System Account user was successfully updated.');
            return redirect()->route('sysadmin.user.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::id() === $id){
            session()->flash('alert-error', 'You cannot do a self-destruction, Please goto the Profile Setting the Select Remove Account.');
            return back();
        }
        $user = User::find($id);
        // process deleting the data

        //rempve the user avatar from the disk
        $path = $user->avatar;
        Storage::disk('public')->delete($path);
        // remove or detach to the database relationship
        $user->roles()->detach();
        // remove to the database
        $user->delete();

        session()->flash('alert-success','Account User was successfully deleted.');
        return redirect()->route('sysadmin.user.index');
    }

    public function Deactivate($id)
    {
        if(Auth::id() === $id){
            session()->flash('alert-error', 'You cannot Deactivate/Activate your Account here!, Please goto user profile and edit there.');
            return back();
        }

        $user = User::find($id);
        $user->blocked = 1;
        $user->save();
        session()->flash('alert-success','User successfully Deactivated!');
        return redirect()->route('sysadmin.user.index');
    }

    public function Activate($id)
    {
        if(Auth::id() === $id){
            session()->flash('alert-error', 'You cannot Deactivate/Activate your Account here!, Please goto user profile and edit there.');
            return back();
        }

        $user = User::find($id);
        $user->blocked = 0;
        $user->save();

        session()->flash('alert-success','User successfully Activated!');
        return redirect()->route('sysadmin.user.index');
    }
}
