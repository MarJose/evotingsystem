<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\candidates;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Organization as OrgModel;

class Organization extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orgmodel = OrgModel::all();
        return view('AdminPanel.organization.org-list')
            ->with('orgdb', $orgmodel);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('AdminPanel.organization.add-organization');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
           'orgname' => 'required|unique:organizations,Organization_name',
            'orgdescription' => 'required'
        ]);

        OrgModel::create([
           'Organization_name' => $request->input('orgname'),
           'Organization_description' => $request->input('orgdescription')
        ]);

        session()->flash('alert-success', 'New Organization has been added!');
        return  redirect()->route('sysadmin.organization.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'orgname' => 'required|unique:organizations,Organization_name',
            'orgdescription' => 'required'
        ]);

        $orgdata = OrgModel::find($id);
        $orgdata->update([
            'Organization_name' => $request->input('orgname'),
            'Organization_description' => $request->input('orgdescription')
        ]);

        session()->flash('alert-success', 'Organization has successfully updated!');
        return  redirect()->route('sysadmin.organization.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        OrgModel::find($id)->delete();

        session()->flash('alert-success', 'Organization has successfully deleted!');
        return  redirect()->route('sysadmin.organization.index');
    }
}
