<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\candidates;

use App\Candidates;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Organization;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function index()
    {
        //
        $candidates = Candidates::all();
        return view('AdminPanel.candidates.candidates-list')
            ->with('candidates', $candidates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        $org = Organization::all();

        if(!$org->isEmpty())
        {
            return view('AdminPanel.candidates.register-candidates')
                ->with('organizations', $org);
        }

        session()->flash('alert-warning','There is no Candidates Organization available!, Please add first.');
        return view('AdminPanel.organization.add-organization');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //
        //dd($request);

        $this->validate($request,[
            'candidateImage' => 'required|mimes:jpg,jpeg,png,gif|max:2048|dimensions:width=200,height=200',
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'Grade_Level' => 'required',
            'position' => 'required',
            'organization' => 'required',
            'studentid' => 'required|unique:candidates,StudentID'
        ]);

        $candidateImage = $request->file('candidateImage');
        $extension = $candidateImage->getClientOriginalExtension();
        $path = 'img/candidates/images/'.Str::slug($candidateImage->getFilename().Str::random(12).Carbon::now()->timestamp).'.'.$extension;
       Storage::disk('public')->put($path,  File::get($candidateImage));
        //$candidateImage->store($path, 'public');
        $candidate = Candidates::create([
            'StudentPicture' => $path,
            'StudentID' => $request->input('studentid'),
            'FName' => $request->input('First_Name'),
            'LName' => $request->input('Last_Name'),
            'Position' => $request->input('position'),
            'Gradelvl' => $request->input('Grade_Level')
        ]);

        $candidate->organizations()->attach($request->input('organization'));

        session()->flash('alert-success', 'New Candidates has been added successfully!');
        return redirect()->route('sysadmin.candidates.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $candidates = Candidates::find($id);
        $organizations = Organization::all();
        return view('AdminPanel.candidates.edit-candidate')
            ->with('candidates', $candidates)
            ->with('organizations', $organizations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'Grade_Level' => 'required',
            'position' => 'required'
        ]);
        // determine if has a new image uploaded

        if($request->has('candidateImage')){
            $candidateImage = $request->file('candidateImage');
            $extension = $candidateImage->getClientOriginalExtension();
            $path = 'img/candidates/images/'.Str::slug($candidateImage->getFilename().Str::random(12).Carbon::now()->timestamp).'.'.$extension;
            Storage::disk('public')->put($path,  File::get($candidateImage));
           $candidate = Candidates::where('id', $id)
                ->update([
                    'StudentPicture' => $path,
                    'FName' => $request->input('First_Name'),
                    'LName' => $request->input('Last_Name'),
                    'Position' => $request->input('position'),
                    'Gradelvl' => $request->input('Grade_Level')
                ]);
            $candidate->organizations()->sync($request->input('position'));

            session()->flash('alert-success','Candidates was successfully updated.');
            return redirect()->route('sysadmin.candidates.index');
        }else{
            $candidate =  Candidates::where('id', $id)
                ->update([
                    'FName' => $request->input('First_Name'),
                    'LName' => $request->input('Last_Name'),
                    'Position' => $request->input('position'),
                    'Gradelvl' => $request->input('Grade_Level')
                ]);
            $candidate->organizations()->sync($request->input('position'));

            session()->flash('alert-success','Candidates was successfully updated.');
            return redirect()->route('sysadmin.candidates.index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $candidate = Candidates::find($id);
        if($candidate)
        {
            // remove or detach to the database relationship
            $candidate->organizations()->detach();
            //remove image to the disk
            $path = $candidate->StudentPicture;
            Storage::disk('public')->delete($path);
            // delete in the database
            $candidate->delete();

            session()->flash('alert-success','Candidates was successfully deleted.');
            return redirect()->route('sysadmin.candidates.index');
        }else
        {
            session()->flash('alert-danger','Something went wrong while deleting the candidates.');
            return redirect()->route('sysadmin.candidates.index');
        }
    }
}
