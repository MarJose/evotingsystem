<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2020.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App\Http\Controllers\sysadmin\voters;

use App\Imports\VotersImport;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class voters extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AdminPanel.voters.voters-list')
            ->with('students', Student::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return  view('AdminPanel.voters.Register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'School_Id' => 'required|unique:students,Student_ID',
            'First_Name' => 'required',
            'Last_Name' => 'required',
            'Middle_Name' => 'required',
            'Grade_Level' => 'required',

        ]);

        Student::create([
            'Student_ID' => $request->input('School_Id'),
            'FirstName' => $request->input('First_Name'),
            'LastName' => $request->input('Last_Name'),
            'MiddleName' => $request->input('Middle_Name'),
            'GradeLvl' => $request->input('Grade_Level')
        ]);

        session()->flash('alert-success', 'New voters successfully created.');
        return  redirect()->route('sysadmin.voters.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return the data to the ajax { json response }
        $data = Student::find($id);
        $res = array(
                'Status' => 'passed',
                'Success' => true,
                'result' => $data,
        );
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = Student::find($id);
        $user->update([
            'FirstName' => $request->input('First_Name'),
            'LastName' => $request->input('Last_Name'),
            'MiddleName' => $request->input('Middle_Name'),
            'GradeLvl' => $request->input('Grade_Level')
        ]);

        session()->flash('alert-success', 'Voters / Student #'.$user->Student_ID.' has been updated!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = Student::find($id);

        if(!$user->isActive) {
            $res = Student::where('id', $id)->delete();
            if($res){
                session()->flash('alert-success', 'Voters / Student #'.$user->Student_ID.' has already been deleted!');
                return back();
            }else{
                session()->flash('alert-success', 'Make sure the Current User #'.$user->Student_ID.' is existing in the database.');
                return back();
            }

        }else{
            session()->flash('alert-success', 'Unable to delete voters / student that is online.');
            return back();
        }

    }

    public function GeneratePassword($id)
    {
        //
        $user = Student::find($id);
        $pass = Str::random(6);
        $user->password = bcrypt($pass);
        $user->save();

        return response()->json([
            'pass' => $pass,
        ]);
    }

    public  function  import(Request $request)
    {
        //
        $this->validate($request,[
            'import_file' => 'mimetypes:text/csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel|required'
        ]);

        $bulkdata = Excel::toCollection(new VotersImport(), $request->file('import_file'));

        foreach($bulkdata as $voter )
        {
            foreach($voter as $data){
                Student::updateOrCreate([
                    'Student_ID' => $data['school_id']
                ],
                    [
                        'FirstName' => $data['first_name'],
                        'LastName' => $data['last_name'],
                        'MiddleName' => $data['middle_name'],
                        'GradeLvl' => $data['grade_level'],

                    ]);
            }
        }
        session()->flash('alert-success', 'Bulk import successfully save and update for the duplicates.');
        return redirect()->route('sysadmin.voters.index');
    }

    public function downloadfile(){
        //return Storage::download('students_201909180945.xlsx',)
        return Storage::disk('public')->download('storage/files/students_201909180945.xlsx','voters-student_201909180945.xlsx', ['Content-Description' =>  'File Transfer','Content-Type' => 'application/octet-stream','Content-Disposition' => 'attachment; filename=voters-student_201909180945.xlsx']);
    }
}
