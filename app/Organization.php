<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Organization extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //
    protected $auditInclude = [
        'Organization_name', 'Organization_description',
    ];

    protected $fillable = [
      'Organization_name' ,
        'Organization_description'
    ];

    public function candidates(){
        return $this->belongsToMany(Candidates::class);
    }
}
