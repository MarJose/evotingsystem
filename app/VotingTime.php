<?php
/*******************************************************************************
 * @Author MarJose Darang - Copyright (c) 2019.
 *
 * @Licenses GNU GPLv3
 *  https://choosealicense.com/licenses/gpl-3.0/
 *
 ******************************************************************************/

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class VotingTime extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;
    //
    protected $auditInclude = [
        'isopen', 'isOpenLiveMonitor', 'date_start','time_start'
    ];
    protected $fillable = [
        'isopen', 'isOpenLiveMonitor', 'date_start','time_start'
    ];
}
