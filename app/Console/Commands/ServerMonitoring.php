<?php

namespace App\Console\Commands;

use App\Http\Controllers\sysadmin\sysinfo\info;
use App\Traits\SystemInfo;
use Illuminate\Console\Command;
use App\Events\ServerMonitor;

class ServerMonitoring extends Command
{
    use SystemInfo;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'run and retrieve all the physical information of the server in realtime';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        event(new ServerMonitor($this->sysinfo()));
    }
}
