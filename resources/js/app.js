/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import CxltToastr from 'cxlt-vue2-toastr';
let toastrConfigs = {
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEs0lEQVRoQ9WajbEMQRDHuyNABIgAESACROCJABEgAkSACBABIvBEgAgQQavfVvdW39zszn7MKW+qrt6ru9uZ/k93//vrVE64zOymiFziCFX9csKjRHtsbmbXROS2iNwREYTnNbV+iMi5iHwWkS+qyv+712YgZnZZRB6KyFlD8JaQAPsgIq9Vlf83rdVAHMBjEXkiIoCJ9dNvmRvm9TvftmsNzfFCY2jvRiH1WxF5sQXQKiBmhgZeJQB/RITD324xEQeHRnldTaCeu4Z+L1XPIiCuhTcict83BgCAXqnq4sPmhDIzwAAgAKHVR0svqAnEmee9mwSyvMOsegEowZkZYDBb2I5LeqqqaH12zQJxEJ/clNDCmarimCddfi7Chw+hmVkwk0AqIO4sVXMPlG7OUPQiMFUgBYhvMMypTKkF2szQBCTDmtTMERC/CcwJisScbm6hw5aAaz43MxwfzeAzt2ry1IDARsQJFg91ibxrBK8QAPEKOWC0c1W9VX7nAIiZEaTQBgu2ANR/sdzcv7owBE3YbVwlEL6ISZEDAWr3SpkAcYKbhfW4pNXxx6n5Wc3ERiAekAh6XU3KzNgTEHl9VtW7a2/JLyVM7J2qjvtmIJgUWjj4wtrDKvZtE3tc30Ii6cLRKHsMmh2AFPa36YApwGY2BeTKFvNyeRGeyD/6cQAJpvqmqnO1xGoFmRnpxsviQVJ23t+0zCzkHRksgHz3XOokTGVmJJu8BmdvpRstdDULUk+lAdLVyVvC7P3czMK8hmgPEDwfZvmpqhQ9F2KZGTR+L8gJIAQWuLln7MDPOCT8DcqEDTeXshU2PJAbIIFslwM6m+AHOPaUZh/0KgPc76iTKKmvAIRUmQ7IUdhfa2Oeqcatl+Ur2w2Hrt239v2cTimrJ5B8YLqxAzk486IByQnoKPtFBBJsmBXQjRn/pWnlyi7AdMvjakDiwG6HOIP9Khp4vN2TtSL1GdKqU8aRKIKyaW1OFJfEkdEpOzpiLpdDho+qGg2+3cRVsm0kjZFq31VV4squZWZRaeZ9uiWkXmBhuqxB5gBykLfsQVEckrfqcknuf8GIf1R1aKQHkPhgd+QtGhjd44cDObr4AAKqUFWzPTmnsSkgvVpLRdkxajnX7EHDP1T1+k7zilohb8N70SPDJDY5fuo8HgTXDISMNQqsXY7pFRykMcwP06JziVkwjljd+Cu0fWA5ZV8rcvzJ1uRSTbnTx2SKx2gB7WLExIZHtVMJJLcmN/WelgJd+71UAPLoUSu31vvNWSsjtUdrD+39/aJ5WK2bpsYKYWLItIvF9oIqRhyT5fjcoGfRXGKvoA0qx8diYjY7p2mN3jKYf2pmhTk1h03NsrOYGEGZpOLduiGlRpztaGBEg7oJYkxRWuZRMAbUTBx40Xpu7eeVOf5HH8A2RxBNjYQwHowwtZiDD4DWDvZr4BwABBNtJAInI/DmWDr2WwwkAcpz8HibaM0LVmmanZsPLajcE469Ns3xVwPx7JPASakZg/180QCJXwCVJkGM4tlaxx8Az5dcRE2rm4Dkjbx/hWMiZJlbtdyE33BhPnTom34wt9luIAWo+K1W/Aoot04j+x3+7s27SlB/AfLvdlGxlz0xAAAAAElFTkSuQmCC',
    position: 'top right',
    showDuration: 300,
    hideDuration: 2000,
    closeButton: false,
    delay: 0,
    timeOut: 1500,
    color: '#2F96B4',
    showMethod: 'fadeInRight',
    hideMethod: 'fadeOut',

};
Vue.use(CxltToastr, toastrConfigs);
Vue.use(require('vue-moment'));




//Vue.component('server-component', require('./components/ServerComponent').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const notification_app = new Vue({
    el: '#notification_app',
});
const dashboard_app = new Vue({
    el: '#dashboard_app',
});
