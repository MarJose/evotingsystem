@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="clearfix"></div>
            <!-- Validation Data Response error-->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- Validation Data Response error-->

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>List of Account System User</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                {{-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                 </li>--}}
                                <a href="{{ url(route('sysadmin.user.create')) }}"><i class="fa fa-user-plus fa-2x"></i></a>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box ">
                                        <table id="" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                            <thead>
                                            <tr>
                                                {{--<th><input type="checkbox" id="checkall" class="flat"> Select All</th>--}}
                                                <th>No.</th>
                                                <th>Full Name</th>
                                                <th>Account Name</th>
                                                <th>Email</th>
                                                <th>Account Role</th>
                                                <th>Account Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    {{-- <td><input type="checkbox" class="flat chck"></td>--}}
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $user->Firstname }} {{ $user->Lastname }}</td>
                                                    <td>{{ $user->username }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->roles()->get()->pluck('role_name')->first() }}</td>
                                                    <td>
                                                        @if ($user->blocked === 0)
                                                            Activated
                                                        @else
                                                            Deactivated
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('sysadmin.user.edit', $user->id) }}" type="button" class="btn btn-primary btn-round"><i class="fa fa-edit" data-toggle="tooltip" title="Edit user"></i></a>
                                                        <button type="button" class="btn btn-success btn-round" onclick="activate({{ $user->id }})" data-toggle="tooltip" title="Activate user"><i class="fa fa-user fa-check"></i></button>
                                                        <button type="button" class="btn btn-warning btn-round" onclick="deactivate({{ $user->id }})" data-toggle="tooltip" title="Deactivate user"><i class="fa fa-ban"></i></button>
                                                        <button type="button" class="btn btn-danger btn-round" onclick="deleteData( {{ $user->id }})" data-toggle="tooltip" title="Delete user"><i class="fa fa-trash-o"></i></button>
                                                        <form action="{{ route('sysadmin.user.destroy', $user->id) }}" method="POST" id="delete_{{ $user->id }}">
                                                            {{ method_field('DELETE') }}
                                                            @csrf
                                                        </form>
                                                        <form action="{{ route('sysadmin.user.activate', $user->id) }}" method="POST" id="activate_{{ $user->id }}">
                                                            @csrf
                                                        </form>
                                                        <form action="{{ route('sysadmin.user.deactivate', $user->id) }}" method="POST" id="deact_{{ $user->id }}">
                                                            @csrf
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->


    <script>
        function deleteData(id){
            const uid = id;
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-round btn-lg',
                    cancelButton: 'btn btn-danger btn-round btn-lg'
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    //delete that ***
                    $("#delete_"+id).submit();
                }

            })
        }

        function deactivate(id){
            const uid = id;
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-round btn-lg',
                    cancelButton: 'btn btn-danger btn-round btn-lg'
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to deactivate/ban this user.",
                type: 'warning',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    //ban that ***
                    $("#deact_"+id).submit();
                }

            })
        }
        function activate(id){
            const uid = id;
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-round btn-lg',
                    cancelButton: 'btn btn-danger btn-round btn-lg'
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to activate/ removed from being banned to use the system.",
                type: 'question',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    //activate that ***
                    $("#activate_"+id).submit();
                }

            })
        }

        $(document).ready(function(){

            $(".keytable").DataTable({
                responsive: true,
                keys: !0,
                dom: "Bfrtip",
                buttons: [{
                    extend: "copyHtml5",
                    className: "btn-sm btn-round"
                }, {
                    extend: "csvHtml5",
                    className: "btn-sm btn-round"
                }, {
                    extend: "print",
                    className: "btn-sm btn-round"
                }],
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

@endsection
