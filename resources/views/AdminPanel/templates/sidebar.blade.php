<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href=" {{ route('sysadmin.dashboards') }}" class="site_title"><i class="fa fa-paw"></i> <span>eVote System</span></a>
        </div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                @if(Auth::user()->avatar)
                    <img src="{{ Auth::user()->avatar }}" alt="profile_image" class="img-circle profile_img">
                @endif

            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{Auth::user()->Firstname}}, {{ Auth::user()->Lastname }}</h2>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a href="{{ url(route('sysadmin.dashboards')) }}"><i class="fa fa-home"></i> Dashboard</a></li>
                    @can('voters section')
                    <li><a><i class="fa fa-edit"></i> Voter / Student <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('add voters')
                            <li><a href="{{ url(route('sysadmin.voters.create')) }}">Register</a></li>
                            @endcan
                           @can('view voters')
                            <li><a href="{{ url(route('sysadmin.voters.index')) }}">Voters / Student List</a></li>
                           @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('candidate section')
                    <li><a><i class="fa fa-users"></i> Candidates <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('add candidate')
                            <li><a href="{{ route('sysadmin.candidates.create') }}">Register</a></li>
                            @endcan
                            @can('view candidate')
                            <li><a href="{{ route('sysadmin.candidates.index') }}">Candidates List</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('organization section')
                    <li><a><i class="fa fa-table"></i> Organization <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('add organization')
                            <li><a href="{{ route('sysadmin.organization.create') }}">Create new Organization</a></li>
                            @endcan
                            @can('view organization')
                            <li><a href="{{ route('sysadmin.organization.index') }}">Organization List</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('user section')
                    <li><a><i class="fa fa-user-secret"></i> System User <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @can('add user')
                            <li><a href="{{ route('sysadmin.user.create') }}">Create new user</a></li>
                            @endcan
                            @can('view users')
                            <li><a href="{{ route('sysadmin.user.index') }}">Users list</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('system section')
                    <li><a><i class="fa fa-cogs"></i> System Setting <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @if(auth()->user()->can('configure live monitor') || auth()->user()->can('configure precinct door'))
                            <li><a href="{{ route('sysadmin.general.edit',1) }}">General</a></li>
                            @endif
                            @can('truncate database')
                            <li><a href="{{ route('sysadmin.database.page') }}">Database Setting</a></li>
                            @endcan
                            <li><a href="{{ route('sysadmin.seats.index') }}">Seats</a></li>
                        </ul>
                    </li>
                    @endcan
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings" href="{{ route('sysadmin.general.edit',1) }}">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Profile" href="{{ route('sysadmin.profile.index') }}">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit(); ">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                <form id="logout-form" action="{{ route('sysadmin.logout') }}" method="POST" style="display: none;">
                    @csrf
                    @method('POST')
                </form>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
