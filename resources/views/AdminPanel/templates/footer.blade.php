<!-- footer content -->
<footer class="footer_fixed">
    <div class="pull-right">
        <i class="fa fa-paw fa-2x"></i> eVote System - All right reserved &copy; 2019 &dash; MarJose Darang
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
