<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    {{--
    |
    | ------------------------------------------------------------------------------------------
    | All CSS Libraries
    | ----------------------------------------------------------------------------------------
    |
    |--}}

    <!-- Bootstrap -->
    <link href=" {{ asset('gentelella-1.4.0/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('gentelella-1.4.0/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('gentelella-1.4.0/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('gentelella-1.4.0/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('gentelella-1.4.0/vendors/google-code-prettify/bin/prettify.min.css') }}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('gentelella-1.4.0/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset('gentelella-1.4.0/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset('gentelella-1.4.0/vendors/starrr/dist/starrr.css') }}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('gentelella-1.4.0/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="{{ asset('gentelella-1.4.0/vendors/normalize-css/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella-1.4.0/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella-1.4.0/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="{{ asset('gentelella-1.4.0/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{ asset('gentelella-1.4.0/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella-1.4.0/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella-1.4.0/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella-1.4.0/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gentelella-1.4.0/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <!-- SweetAlert2 -->
    <link href="{{ asset('sweetalert2/@sweetalert2/themes/dark/dark.css') }}" rel="stylesheet">

    <!-- FileInput -->
    <link href="{{ asset('bootstrap-fileinput-4/css/fileinput.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap-fileinput-4/css/fileinput-rtl.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('gentelella-1.4.0/build/css/custom.min.css') }}" rel="stylesheet">



    {{--
    |
    | ------------------------------------------------------------------------------------------------------
    | All Javascript Libraries
    | -----------------------------------------------------------------------------------------------------
    |
    --}}
    <script defer src="{{ mix('js/app.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery/dist/jquery.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('gentelella-1.4.0/vendors/bootstrap/dist/js/bootstrap.js') }}"></script>
    {{-- Smart resize--}}
    <script src="{{ asset('gentelella-1.4.0/src/js/helpers/smartresize.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('gentelella-1.4.0/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('gentelella-1.4.0/vendors/nprogress/nprogress.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset('gentelella-1.4.0/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('gentelella-1.4.0/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('gentelella-1.4.0/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('gentelella-1.4.0/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('gentelella-1.4.0/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('gentelella-1.4.0/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('gentelella-1.4.0/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('gentelella-1.4.0/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"></script>
    <!-- starrr -->
    <script src="{{ asset('gentelella-1.4.0/vendors/starrr/dist/starrr.js') }}"></script>
    <!-- Chart.js -->
    <script src="{{ asset('gentelella-1.4.0/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('gentelella-1.4.0/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('gentelella-1.4.0/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('gentelella-1.4.0/vendors/DateJS/build/date.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('gentelella-1.4.0/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- Datatables -->
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-keytable/js/dataTables.keyTable.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-responsive/js/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('gentelella-1.4.0/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
    <!-- Ion.RangeSlider -->
    <script src="{{ asset('gentelella-1.4.0/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="{{ asset('gentelella-1.4.0/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <!-- jquery.inputmask -->
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- jQuery Knob -->
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('sweetalert2/dist/sweetalert2.min.js') }}"></script>

    <!-- Fileinput -->
    <script src="{{ asset('bootstrap-fileinput-4/js/fileinput.js') }}"></script>
    <script src="{{ asset('bootstrap-fileinput-4/js/plugins/piexif.js') }}"></script>
    <script src="{{ asset('bootstrap-fileinput-4/js/plugins/purify.js') }}"></script>


</head>

<body class="nav-md">

<div class="container body" >
    <div class="main_container" >
        <!-- Side Bar Navigation -->
    @include('AdminPanel.templates.sidebar')
            <!-- Top Navigation Bar -->
        @include('AdminPanel.templates.navbar')
        <!-- Body Content -->
        @yield('content')
        <!-- Footer -->
        @include('AdminPanel.templates.footer')
    </div>
</div>


<!-- Custom Theme Scripts -->
<script src="{{ asset('gentelella-1.4.0/build/js/custom.min.js') }}"></script>
</body>
</html>
