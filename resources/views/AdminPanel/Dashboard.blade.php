@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    {{--<script>

           /* setInterval(getSysinfo, 2000);

            /!**
             * @return {string}
             *!/
            function cleantime(t)
            {
                let res = t.split(',');
                let hour = res[0].replace(/\D/g,'');
                let minute = res[1].replace(/\D/g,'');
                let second = '00';
                let formattedTime;
                if(typeof  res[2] !== 'undefined'){
                    second = res[2].replace(/\D/g,'');
                }
                if(res.length !== 3){
                    // no hour stated
                    formattedTime = "00:" + hour + ":" + minute;
                }else{
                    formattedTime = hour + ":" + minute + ":" + second;
                }

                return  formattedTime;
            }
            function formatBytes(bytes, decimals = 2) {
                if (bytes === 0) return '0 Bytes';

                const k = 1024;
                const dm = decimals < 0 ? 0 : decimals;
                const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

                const i = Math.floor(Math.log(bytes) / Math.log(k));

                return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
            }

            function getSysinfo() {
                              //ajax request query here!
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: " {{ url(route('sysadmin.sysinfopath')) }}",
                    type: "GET",
                    cache:false,
                    success: function(data){
                        //  uptime
                        $('#ServerUptime').text(cleantime(data.Uptime['text']));
                        // Ram
                        $('#ServerMemory').text(formatBytes(data.Ram['free']));
                        // Registered User
                        $('#registeredUser').text(data.Registered_User);
                        // CPU Usage
                        $('#Servercpuload').text(data.CPUusage + ' %');
                        getGraphCPU(data.CPUusage);
                        // HDD
                        getDiskReadGraph(data.Hdd['reads']);
                        getDiskWriteGraph(data.Hdd['writes']);

                    }
                });




            }*/

        /*// System Graph Real time
            let cpu = [];
           function getGraphCPU(usage){
               cpu.push(usage);
               $(".cpuusage").sparkline(cpu, {
                   type: "line",
                   height: "40",
                   width: "200",
                   lineColor: "#26B99A",
                   fillColor: "#ffffff",
                   lineWidth: 3,
                   spotColor: "#34495E",
                   minSpotColor: "#34495E",
                   tooltipSuffix: 'CPU Usage'
               });
           }
           let diskwrite = [];
           function getDiskWriteGraph(usage){
               diskwrite.push(usage);
               $(".diskwrite").sparkline(diskwrite, {
                   type: "line",
                   height: "40",
                   width: "200",
                   lineColor: "#26B99A",
                   fillColor: "#ffffff",
                   lineWidth: 3,
                   spotColor: "#34495E",
                   minSpotColor: "#34495E",
                   tooltipSuffix: 'Write per seconds'
               });
           }
            let diskread = [];
            function getDiskReadGraph(usage){
                diskread.push(usage);
                $(".diskread").sparkline(diskread, {
                    type: "line",
                    height: "40",
                    width: "200",
                    lineColor: "#11b91a",
                    fillColor: "#ffffff",
                    lineWidth: 3,
                    spotColor: "#34495E",
                    minSpotColor: "#34495E",
                    tooltipSuffix: 'Read per seconds'
                });
            }*/

    </script>--}}

       <!-- page content -->
    <div class="right_col" role="main" id="dashboard_app">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_content">
                        <div class="x_title">
                            <i class="fa fa-paw"></i> eVote System Voting Information
                        </div>
                    </div>
                </div>
            </div>
            @php
            use App\Student;
            @endphp
            <div class="row top_tiles">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-users"></i></div>
                        <div class="count">{{ Student::all()->count()}}</div>
                        <h3>Voters / Students</h3>
                        <p>Overall Voters / Student Registered</p>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-comments-o"></i></div>
                        <div class="count">{{ Student::where('doneVoting',1)->count() }}</div>
                        <h3>Done Votes</h3>
                        <p>Overall Student / Voters Done voting.</p>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                        <div class="count">{{ Student::where('isActive',1)->count() }}</div>
                        <h3>Active Voters</h3>
                        <p>Current voting on the precinct</p>
                    </div>
                </div>
            </div>
            {{-- Server System information--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="x_content">
                        <div class="x_title">
                            <i class="fa fa-server"></i> Real-Time Server System Information
                        </div>
                    </div>
                </div>
            </div>
            <server-component></server-component>


        </div>
    </div>
    <!-- /page content -->
@endsection
