@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
        <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        {{-- Added Validation Error here--}}
        @include('partials.validation-message')
        {{-- /. End of Validation Error here--}}

        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>General Setting</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <div class="col-md-3 col-xs-12 widget">
                                <div class="x_panel fixed_height_390">
                                    <div class="x_title">
                                        <h2>Precinct Door</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <form class="form" id="precinctdoor"  action="{{ route('sysadmin.general.update',1) }}" method="post">
                                            {{ method_field('PUT') }}
                                            @csrf
                                            <!-- Toggle Button -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Close</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="">
                                                        <label>
                                                            <input type="checkbox" class="js-switch"  name="precinctDoorSwitch"
                                                            {{ $votingTime->isopen ? 'checked':'' }}
                                                            /> Open
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.Toggle Button -->
                                            <br/><br/>
                                            <!-- Date will be open date and time -->
                                            <div class='col-sm-12 col-md-12 col-lg-12'>
                                                <div class="form-group">
                                                    <div class='input-group date' id='datetimepicker3'>
                                                        <input type='text' class="form-control" name="precinctDoorDate" value="{{ $votingTime->date_start }}"/>
                                                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#datetimepicker3').datetimepicker({
                                                        format: 'YYYY/MM/DD'
                                                    });
                                                });
                                            </script>
                                            <div class='col-sm-12 col-md-12 col-lg-12'>
                                                <div class="form-group">
                                                    <div class='input-group date' id='datetimepicker1'>
                                                        <input type='text' class="form-control" name="precinctDoorTime" value="{{ $votingTime->time_start }}" />
                                                        <span class="input-group-addon">
                                                        <span class="fa fa-clock-o"></span>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#datetimepicker1').datetimepicker({
                                                        format: 'hh:mm'
                                                    });
                                                });
                                            </script>


                                            <!-- /. Date will be open date and time -->
                                            <!-- Save Button-->
                                            <br/><br/><br/>
                                            <button type="submit" form="precinctdoor" name="precinctdoorupdate" class="btn btn-success"> Save</button>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12 widget">
                                <div class="x_panel fixed_height_390">
                                    <div class="x_title">
                                        <h2>Live Monitor</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <form class="form" id="livemonitor"  action="{{ route('sysadmin.general.update',1) }}" method="post">
                                        {{ method_field('PUT') }}
                                        @csrf
                                            <!-- Toggle Button -->
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Close</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="">
                                                        <label>
                                                            <input type="checkbox" class="js-switch" name="livemonitorswitch"
                                                                   {{ $votingTime->isOpenLiveMonitor ? 'checked':'' }}
                                                                    /> Open
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.Toggle Button -->

                                            <!-- Save Button-->
                                            <br/><br/><br/>
                                            <button type="submit" form="livemonitor" name="livemonitorupdate" class="btn btn-success"> Save</button>
                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <script>

    </script>

@endsection
