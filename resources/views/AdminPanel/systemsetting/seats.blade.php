@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        {{-- Added Validation Error here--}}
        @include('partials.validation-message')
        {{-- /. End of Validation Error here--}}

        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Seats / Position Availability Configuration</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <form method="post" action="{{ route('sysadmin.seats.update',1) }}" enctype="multipart/form-data"  data-parsley-validate class="form-horizontal form-label-left">
                                {{ method_field('PUT') }}
                                @csrf

                                <div class="row col-lg-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="president">President <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="president" required="required" class="form-control col-md-7 col-xs-12" name="president" value="{{ old('president', optional($seats[0])->president) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vpresident">Vice President <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="vpresident" required="required" class="form-control col-md-7 col-xs-12" name="vpresident" value="{{ old('vpresident', optional($seats[0])->vice_president) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="secretary">Secretary <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="secretary" required="required" class="form-control col-md-7 col-xs-12" name="secretary" value="{{ old('secretary', optional($seats[0])->secretary) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="treasurer">Treasurer <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="treasurer" required="required" class="form-control col-md-7 col-xs-12" name="treasurer" value="{{ old('treasurer', optional($seats[0])->treasurer) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="auditor">Auditor <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="auditor" required="required" class="form-control col-md-7 col-xs-12" name="auditor" value="{{ old('auditor', optional($seats[0])->auditor) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pio">Public Information Officer <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="pio" required="required" class="form-control col-md-7 col-xs-12" name="pio" value="{{ old('pio', optional($seats[0])->pio) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bm">Business Manager <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="bm" required="required" class="form-control col-md-7 col-xs-12" name="bm" value="{{ old('bm', optional($seats[0])->business_manager) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="senoirrep">Senior Representative <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="senoirrep" required="required" class="form-control col-md-7 col-xs-12" name="senoirrep" value="{{ old('senoirrep', optional($seats[0])->senior_representative) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="juniorrep">Junior Representative <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="juniorrep" required="required" class="form-control col-md-7 col-xs-12" name="juniorrep" value="{{ old('juniorrep', optional($seats[0])->junior_representative) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <script>

    </script>

@endsection
