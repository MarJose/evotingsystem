@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        {{-- Added Validation Error here--}}
        @include('partials.validation-message')
        {{-- /. End of Validation Error here--}}

        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>General Setting</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <i class="fa fa-exclamation-triangle fa fa-2x"></i>
                                <strong>Warning! </strong>
                                If you don't know what you're doing, please leave this page!.
                            </div>


                            <div class="col-md-6 col-lg-6 col-xs-6 widget_tally_box">
                                <div class="x_panel ">
                                    <div class="x_title">
                                        <h2>Database Tables</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        {{--<ul class="list-group">
                                            @foreach($tables as $index => $table)
                                            <li class="list-group-item">{{ $table }}<span class="badge"> {{ $tablesCount[$index] }}</span></li>
                                            @endforeach
                                        </ul>--}}
                                        <div class="table-responsive">
                                            <table class="table jambo_table">
                                                <thead>
                                                <tr class="headings">
                                                    <th class="column-title">Table Name</th>
                                                    <th class="column-title">Table Entry</th>
                                                    <th class="column-title no-link last">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($tables as $index => $table)
                                                <tr>
                                                    <td class=" ">{{ $table }}</td>
                                                    <td class=" "><span class="badge bg-blue-sky"> {{ $tablesCount[$index] }}</span></td>
                                                    <td class=" last">
                                                        <a href="#" onclick="truncate('{{ route('sysadmin.database.truncate', $table) }}')">Truncate</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <script>
        function truncate(route)
        {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-round',
                    cancelButton: 'btn btn-danger btn-round'
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Truncate Table',
                text: "Do you want to truncate this table?",
                type: 'question',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonText: 'Yes, Truncate this table',
                cancelButtonText: 'No, Cancel!',
                showLoaderOnConfirm: true,
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    //delete that ***
                    window.location.href = route;
                }

            });

        }

    </script>
@endsection

