@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        {{-- Added Validation Error here--}}
        @include('partials.validation-message')
        {{-- /. End of Validation Error here--}}

        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Create new Organization</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br />
                            <form class="form" id="regOrg"  action="{{ route('sysadmin.organization.store') }}" method="post"  data-parsley-validate class="form-horizontal form-label-left">
                                @csrf
                                <!-- Date will be open date and time -->
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orgname">Organization / Club name<span class="required">*</span>
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" id="orgname" required="required" class="form-control col-md-7 col-xs-12" name="orgname" value="{{ old('orgname') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orgdesc">Organization Description<span class="required">*</span>
                                    </label>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" id="orgdesc" required="required" class="form-control col-md-7 col-xs-12" name="orgdescription" value="{{ old('orgdescription') }}">
                                    </div>
                                </div>

                                    <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <script>

    </script>

@endsection
