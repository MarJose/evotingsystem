@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="clearfix"></div>
            <!-- Validation Data Response error-->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- Validation Data Response error-->

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>List of Organization</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                {{-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                 </li>--}}
                                <a href="{{ url(route('sysadmin.organization.create')) }}"><i class="fa fa-user-plus fa-2x"></i></a>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box ">
                                        <table id="" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                            <thead>
                                            <tr>
                                                {{--<th><input type="checkbox" id="checkall" class="flat"> Select All</th>--}}
                                                <th>Organization Name</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($orgdb as $orgdata)
                                                <tr>
                                                    {{-- <td><input type="checkbox" class="flat chck"></td>--}}
                                                    <td>{{ $orgdata->Organization_name }}</td>
                                                    <td>{{ $orgdata->Organization_description }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary btn-round"  data-toggle="modal" data-target="#edit-modal" data-orgname="{{ $orgdata->Organization_name }}" data-orgdescription="{{ $orgdata->Organization_description }}" data-route="{{ route('sysadmin.organization.update', $orgdata->id) }}" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit Organization"></i></button>
                                                        <button type="button" class="btn btn-danger btn-round" onclick="deleteData( {{ $orgdata->id }})" data-toggle="tooltip" title="Delete Organization"><i class="fa fa-trash-o"></i></button>
                                                        <form action="{{ route('sysadmin.organization.destroy', $orgdata->id) }}" method="POST" id="{{ $orgdata->id }}">
                                                            {{ method_field('DELETE') }}
                                                            @csrf
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Import -->
    @include('AdminPanel.organization.modal.edit-org')
    <!-- /.Modal Import -->

    <!-- /page content -->
    <script>
        $('#edit-modal').on('show.bs.modal', function(e){

            const route = $(e.relatedTarget).data('route');

            // adding the value to the form
            $('#orgname').val($(e.relatedTarget).data('orgname'));
            $('#orgdesc').val($(e.relatedTarget).data('orgdescription'));
            //attached a dynamic URL action to the form
            $('#editForm').attr('action', route);

        });

        function deleteData(id){
            const uid = id;
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-round btn-lg',
                    cancelButton: 'btn btn-danger btn-round btn-lg'
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    //delete that ***
                    $("#"+id).submit();
                }

            })
        }

        $(document).ready(function(){

            $(".keytable").DataTable({
                responsive: true,
                keys: !0,
                dom: "Bfrtip",
                buttons: [{
                    extend: "copyHtml5",
                    className: "btn-sm btn-round"
                }, {
                    extend: "csvHtml5",
                    className: "btn-sm btn-round"
                }, {
                    extend: "print",
                    className: "btn-sm btn-round"
                }],
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

@endsection
