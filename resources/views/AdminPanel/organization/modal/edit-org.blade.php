
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Edit Organization</h4>
            </div>
            <div class="modal-body">
                <form  id="editForm" action="" method="post">
                    {{ method_field('PUT') }}
                  @csrf
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orgname">Organization / Club name<span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" id="orgname" required="required" class="form-control col-md-7 col-xs-12" name="orgname" value="{{ old('orgname') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orgdesc">Organization Description<span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" id="orgdesc" required="required" class="form-control col-md-7 col-xs-12" name="orgdescription" value="{{ old('orgdescription') }}">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success" form="editForm"><i class="fa fa-upload"></i> Update</button>
            </div>
        </div>
    </div>
</div>

