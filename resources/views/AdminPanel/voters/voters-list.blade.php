@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
     <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="clearfix"></div>
            <!-- Validation Data Response error-->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- Validation Data Response error-->

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>List of Voters / Student</h2>
                            <ul class="nav navbar-right panel_toolbox">
                               {{-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>--}}
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user-plus fa-2x"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ url(route('sysadmin.voters.create')) }}"><i class="fa fa-user-plus"></i> Add Voters</a>
                                        </li>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target=".importuser"><i class="fa fa-upload"></i> Bulk upload Voters / Students</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('sysadmin.VotersFileFormatImport') }}"><i class="fa fa-download"></i> Download Bulk file format</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box ">
                                        <table id="" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                            <thead>
                                            <tr>
                                                {{--<th><input type="checkbox" id="checkall" class="flat"> Select All</th>--}}
                                                <th>School ID</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Middle Name</th>
                                                <th>Grade Level</th>
                                                <th>Done Voting?</th>
                                                <th>is Active?</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($students as $student)
                                                <tr>
                                                   {{-- <td><input type="checkbox" class="flat chck"></td>--}}
                                                    <td>{{ $student->Student_ID }}</td>
                                                    <td>{{ $student->FirstName }}</td>
                                                    <td>{{ $student->LastName }}</td>
                                                    <td>{{ $student->MiddleName }}</td>
                                                    <td>{{ $student->GradeLvl }}</td>
                                                    <td>
                                                        @if($student->doneVoting === 1)
                                                            Yes
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($student->isActive === 1)
                                                            Yes
                                                        @else
                                                            No
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-info btn-round" onclick="GenPassword({{ $student->id }})" data-toggle="tooltip" title="Generate Password"><i class="fa fa-refresh"></i></button>
                                                        <button type="button" class="btn btn-primary btn-round"  data-toggle="modal" data-target="#edituser-modal" data-middlename="{{ $student->MiddleName }}" data-gradelvl="{{ $student->GradeLvl }}" data-lastname="{{ $student->LastName }}" data-firstname="{{ $student->FirstName }}" data-studentid="{{ $student->Student_ID }}" data-id="{{ $student->id }}" data-route="{{ route('sysadmin.voters.update', $student->id) }}" ><i class="fa fa-edit" data-toggle="tooltip" title="Edit Voter / Student"></i></button>
                                                        <button type="button" class="btn btn-danger btn-round" onclick="deleteVoter( {{ $student->Student_ID }})" data-toggle="tooltip" title="Delete Voter / Student"><i class="fa fa-trash-o"></i></button>
                                                        <form action="{{ route('sysadmin.voters.destroy', $student->id) }}" method="POST" id="{{ $student->Student_ID }}">
                                                            {{ method_field('DELETE') }}
                                                            @csrf
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Import -->
    @include('AdminPanel.voters.modals.importUser')
     @include('AdminPanel.voters.modals.editvoters')
    <!-- /.Modal Import -->

    <!-- /page content -->
<script>
       $('#edituser-modal').on('show.bs.modal', function(e){

           const route = $(e.relatedTarget).data('route');

           // adding the value to the form
           $('#uid').val($(e.relatedTarget).data('id'));
           $('#first-name').val($(e.relatedTarget).data('firstname'));
           $('#last-name').val($(e.relatedTarget).data('lastname'));
           $('#middle-name').val($(e.relatedTarget).data('middlename'));
           $('#gradelvl').val($(e.relatedTarget).data('gradelvl'));
           //attached a dynamic URL action to the form
           $('#edituserForm').attr('action', route);

       });
   function GenPassword(id) {
            //using SweetAlert
       const uid = id;
       const route = "{{ route('sysadmin.generateRndPassword', $student->id) }}";
       const swalWithBootstrapButtons = Swal.mixin({
           customClass: {
               confirmButton: 'btn btn-success btn-round btn-lg',
               cancelButton: 'btn btn-danger btn-round btn-lg'
           },
           buttonsStyling: false
       });
       swalWithBootstrapButtons.fire({
           title: 'Are you sure?',
           text: "Generate Random Password for this Student / Voter",
           type: 'question',
           allowEscapeKey: false,
           allowOutsideClick: false,
           showCancelButton: true,
           confirmButtonText: 'Yes, Generate new password',
           cancelButtonText: 'No, Cancel!',
           showLoaderOnConfirm: true,
           reverseButtons: true,
       }).then((result) => {
           if (result.value) {
               //delete that ***
                $.ajax({
                    type: "GET",
                    url: route,
                    success:function (event) {
                        Swal.fire(
                            'New Password:',
                            event['pass'],
                            'success'
                        )
                    }
                });
           }

       })
       }
   function deleteVoter(id){
       const uid = id;
       const swalWithBootstrapButtons = Swal.mixin({
           customClass: {
               confirmButton: 'btn btn-success btn-round btn-lg',
               cancelButton: 'btn btn-danger btn-round btn-lg'
           },
           buttonsStyling: false
       });
       swalWithBootstrapButtons.fire({
           title: 'Are you sure?',
           text: "You won't be able to revert this!",
           type: 'warning',
           allowEscapeKey: false,
           allowOutsideClick: false,
           showCancelButton: true,
           confirmButtonText: 'Yes, delete it!',
           cancelButtonText: 'No, cancel!',
           reverseButtons: true,
       }).then((result) => {
           if (result.value) {
               //delete that ***
               $("#"+id).submit();
           }

       })
   }

   $(document).ready(function(){

       $(".keytable").DataTable({
           responsive: true,
           keys: !0,
           dom: "Bfrtip",
           buttons: [{
               extend: "copyHtml5",
               className: "btn-sm btn-round"
           }, {
               extend: "csvHtml5",
               className: "btn-sm btn-round"
           }, {
               extend: "print",
               className: "btn-sm btn-round"
           }],
       });
       $('[data-toggle="tooltip"]').tooltip();
   });
</script>

@endsection
