
<div class="modal fade" id="edituser-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Edit Profile</h4>
            </div>
            <div class="modal-body">
                <form  id="edituserForm" action="" method="post">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <input type="hidden" id="uid" hidden/>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="First_Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            <input type="text" id="last-name" name="Last_Name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Middle Name / Initial <span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            <input id="middle-name" class="form-control col-md-7 col-xs-12" required="required" type="text" name="Middle_Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gradelvl" class="control-label col-md-3 col-sm-3 col-xs-12">Grade Level <span class="required">*</span>
                        </label>
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            <input id="gradelvl" class="form-control col-md-7 col-xs-12" type="text" name="Grade_Level" required="required">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success" form="edituserForm"><i class="fa fa-upload"></i> Update</button>
            </div>
        </div>
    </div>
</div>

