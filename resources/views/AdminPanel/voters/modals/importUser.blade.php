
<div class="modal fade importuser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Import Voters / Student</h4>
            </div>
                <div class="modal-body">
                    <form method="post" id="importUser" action="{{ route('sysadmin.VotersImport') }}" enctype="multipart/form-data">
                        {{ method_field('POST') }}
                        {{ csrf_field() }}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-group">
                           {{-- <label for="exampleInputFile">Import</label>--}}
                            <input type="file" id="exampleInputFile" name="import_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, .csv">
                            <p class="help-block">Only accepted format: .xls, .csv, .xlsx</p>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-success" form="importUser"><i class="fa fa-upload"></i> Import</button>
                </div>
        </div>
    </div>
</div>
<script>
    $('#inputGroupFile02').on('change',function(){
        //get the file name
        let fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
