@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="clearfix"></div>
            <!-- Validation Data Response error-->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif
        <!-- Validation Data Response error-->

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>List of Candidates</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                {{-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                 </li>--}}
                                <a href="{{ url(route('sysadmin.candidates.create')) }}"><i class="fa fa-user-plus fa-2x"></i></a>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box ">
                                        <table id="" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                            <thead>
                                            <tr>
                                                {{--<th><input type="checkbox" id="checkall" class="flat"> Select All</th>--}}
                                                <th>Student ID</th>
                                                <th>Full Name</th>
                                                <th>Grade Level</th>
                                                <th>Position</th>
                                                <th>Organization</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($candidates as $candidate)
                                                <tr>
                                                    {{-- <td><input type="checkbox" class="flat chck"></td>--}}
                                                    <td>{{ $candidate->StudentID }}</td>
                                                    <td>{{ $candidate->FName }} {{ $candidate->LName }}</td>
                                                    <td>{{ $candidate->Gradelvl }}</td>
                                                    <td>{{ $candidate->Position }}</td>
                                                    <td>{{ $candidate->organizations()->get()->pluck('Organization_name')->first() }}</td>
                                                    <td>
                                                        <a href="{{ route('sysadmin.candidates.edit', $candidate->id) }}" type="button" class="btn btn-primary btn-round"><i class="fa fa-edit" data-toggle="tooltip" title="Edit Candidate"></i></a>
                                                        <button type="button" class="btn btn-danger btn-round" onclick="deleteData( {{ $candidate->id }})" data-toggle="tooltip" title="Delete Candidate"><i class="fa fa-trash-o"></i></button>
                                                        <form action="{{ route('sysadmin.candidates.destroy', $candidate->id) }}" method="POST" id="{{ $candidate->id }}">
                                                            {{ method_field('DELETE') }}
                                                            @csrf
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->


    <script>
        function deleteData(id){
            const uid = id;
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success btn-round btn-lg',
                    cancelButton: 'btn btn-danger btn-round btn-lg'
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    //delete that ***
                    $("#"+id).submit();
                }

            })
        }

        $(document).ready(function(){

            $(".keytable").DataTable({
                responsive: true,
                keys: !0,
                dom: "Bfrtip",
                buttons: [{
                    extend: "copyHtml5",
                    className: "btn-sm btn-round"
                }, {
                    extend: "csvHtml5",
                    className: "btn-sm btn-round"
                }, {
                    extend: "print",
                    className: "btn-sm btn-round"
                }],
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

@endsection
