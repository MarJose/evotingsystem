@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')
    <style>
        .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }
        .kv-avatar {
            display: inline-block;
        }
        .kv-avatar .file-input {
            display: table-cell;
            width: 213px;
        }
        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Candidate</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- Added Validation Error here--}}
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{-- /. End of Validation Error here--}}
                            <br />
                            <form method="post" action="{{ route('sysadmin.candidates.update', $candidates->id) }}" enctype="multipart/form-data"  data-parsley-validate class="form-horizontal form-label-left" >
                                {{ method_field('PUT') }}
                                @csrf
                                <div class="row col-lg-7 text-center">
                                    <div class=" col-lg-offset-8 text-center">
                                        <div class="kv-avatar">
                                            <div class="file-loading">
                                                <input id="avatar-1" name="candidateImage" type="file" required>
                                            </div>
                                        </div>
                                        <div class="kv-avatar-hint">
                                            <small>Accepted Image format: jpeg, png, bmp, gif</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-lg-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="studentid">Student ID <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="studentid" required="required" class="form-control col-md-7 col-xs-12" name="studentid" value="{{ $candidates->StudentID }}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="First_Name" value="{{ $candidates->FName }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="last-name" name="Last_Name" required="required" class="form-control col-md-7 col-xs-12" value="{{ $candidates->LName }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gradelvl" class="control-label col-md-3 col-sm-3 col-xs-12">Grade Level <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {{--  <input id="gradelvl" class="form-control col-md-7 col-xs-12" type="text" name="Grade_Level" required="required" value="{{ old('Grade_Level') }}">--}}
                                            <select class="form-control"  id="gradelvl" name="Grade_Level">
                                                <option selected value="{{ $candidates->Gradelvl }}">{{$candidates->Gradelvl}}</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pos" class="control-label col-md-3 col-sm-3 col-xs-12">Position<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="pos" class="form-control" name="position">
                                                <option value="president" {{ $candidates->position === 'president' ? 'selected' : '' }}>President</option>
                                                <option value="vice-president" {{ $candidates->position === 'vice-president' ? 'selected' : '' }}>Vice - President</option>
                                                <option value="secretary" {{ $candidates->position === 'secretary' ? 'selected' : '' }}>Secretary</option>
                                                <option value="treasurer" {{ $candidates->position === 'treasurer' ? 'selected' : '' }}>Treasurer</option>
                                                <option value="auditor" {{ $candidates->position === 'auditor' ? 'selected' : '' }}>Auditor</option>
                                                <option value="pio" {{ $candidates->position === 'pio' ? 'selected' : '' }}>Public Information Officer</option>
                                                <option value="business-manager" {{ $candidates->position === 'business-manager' ? 'selected' : '' }}>Business Manager</option>
                                                <option value="senior-representative" {{ $candidates->position === 'senior-representative' ? 'selected' : '' }}>Senior Representative</option>
                                                <option value="junior-representative" {{ $candidates->position === 'junior-representative' ? 'selected' : '' }}>Junior Representative</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Organization</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="organization">
                                                @foreach( $organizations as $org)
                                                    <option value="{{ $org->id }}"
                                                            {{ $candidates->hasOrg($org->Organization_name) ? 'selected' : '' }}
                                                    >
                                                        {{ $org->Organization_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <script>
        $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            showBrowse: false,
            browseOnZoneClick: true,
            removeLabel: '',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src=" {{ asset($candidates->StudentPicture) }}" alt="Your Avatar"><h6 class="text-muted">Click to select</h6>',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
    </script>

@endsection
