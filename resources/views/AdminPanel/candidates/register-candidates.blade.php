@extends('AdminPanel.templates.master_template')

@section('title', 'eVote System')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="" style="padding-top: 80px;">
            {{-- alert Notification blade--}}
            @include('partials.alerts')
            {{-- /alert Notification blade--}}
        </div>
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Register New Candidates</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- Added Validation Error here--}}
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{-- /. End of Validation Error here--}}
                            <br />
                            <form method="post" action="{{ route('sysadmin.candidates.store') }}" enctype="multipart/form-data"  data-parsley-validate class="form-horizontal form-label-left" >
                                @csrf
                                <div class="row col-lg-7 text-center">
                                    <div class=" col-lg-offset-8 text-center">
                                        <div class="kv-avatar">
                                            <div class="file-loading">
                                                <input id="avatar-1" name="candidateImage" type="file" required>
                                            </div>
                                        </div>
                                        <div class="kv-avatar-hint">
                                            <small>Accepted Image format: jpeg, png, bmp, gif</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row col-lg-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="studentid">Student ID <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="studentid" required="required" class="form-control col-md-7 col-xs-12" name="studentid" value="{{ old('studentid') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="First_Name" value="{{ old('First_Name') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="last-name" name="Last_Name" required="required" class="form-control col-md-7 col-xs-12" value="{{ old('Last_Name') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gradelvl" class="control-label col-md-3 col-sm-3 col-xs-12">Grade Level <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          {{--  <input id="gradelvl" class="form-control col-md-7 col-xs-12" type="text" name="Grade_Level" required="required" value="{{ old('Grade_Level') }}">--}}
                                            <select class="form-control"  id="gradelvl" name="Grade_Level">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pos" class="control-label col-md-3 col-sm-3 col-xs-12">Position<span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="pos" class="form-control" name="position">
                                                <option value="president">President</option>
                                                <option value="vice-president">Vice - President</option>
                                                <option value="secretary">Secretary</option>
                                                <option value="treasurer">Treasurer</option>
                                                <option value="auditor">Auditor</option>
                                                <option value="pio">Public Information Officer</option>
                                                <option value="business-manager">Business Manager</option>
                                                <option value="senior-representative">Senior Representative</option>
                                                <option value="junior-representative">Junior Representative</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="org" class="control-label col-md-3 col-sm-3 col-xs-12">Organization</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="org" class="form-control" name="organization">
                                                @foreach( $organizations as $org)
                                                <option value="{{ $org->id }}">{{ $org->Organization_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <script>
        $("#avatar-1").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="fa fa-folder-open"></i>',
            removeIcon: '<i class="fa fa-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="" alt="Your Avatar">',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpeg", "png", "gif", "bmp"]
        });
    </script>

@endsection
