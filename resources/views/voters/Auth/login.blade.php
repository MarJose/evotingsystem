<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="icon" href="{{ asset('img/vote.gif') }}" type="image/x-icon">

    <!-- Site Properties -->
    <title>Login Page - Voting System</title>
    <link rel="stylesheet" type="text/css" href=" {{ asset('semantic/dist/components/reset.css')}}">
    <link rel="stylesheet" type="text/css" href=" {{ asset('semantic/dist/components/site.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/container.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/grid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/header.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/image.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/menu.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/divider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/segment.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/form.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/input.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/button.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/message.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/icon.css') }}">

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/form.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/transition.js') }}"></script>

    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
    </style>
    <script>
        $(document)
            .ready(function() {
                $('.ui.form')
                    .form({
                        fields: {
                            email: {
                                identifier  : 'votersid',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your Voters ID Number'
                                    }/*,
                                    {
                                        type   : 'number',
                                        prompt : 'Please enter a valid Voters ID number'
                                    }*/

                                ]
                            },
                            password: {
                                identifier  : 'passcode',
                                rules: [
                                    {
                                        type   : 'empty',
                                        prompt : 'Please enter your pass code'
                                    }
                                ]
                            }
                        }
                    })
                ;
            })
        ;
    </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
    <div class="column">
        <h2 class="ui teal image header">
            <img src="{{ asset('img/vote.gif') }}" class="image">
            <div class="content">
                Voting System
            </div>
        </h2>
        <form class="ui large form" action="{{ route('voters.login') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
           @csrf
            @method('POST')
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="votersid" placeholder="Voters ID Number" value="{{ old('votersid') }}" autofocus>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="passcode" placeholder="Pass Code">
                    </div>
                </div>
                <div class="ui fluid large grey submit button" id="btn-voters">Login</div>
            </div>

            <div class="ui error message"></div>
            @if( session()->has('error'))
                <div class="ui negative message">
                    <p> {{ session('error') }}</p>
                </div>
            @endif

        </form>
        <div class="ui message">
            Login as System Administrator <a href="{{ route('sysadmin.login.page') }}">Sign in</a>
        </div>
    </div>
</div>

</body>
</html>

