<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @routes

    <title> @yield('title') </title>

    <!-- Bootstrap -->
    <link href=" {{ asset('gentelella-1.4.0/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('gentelella-1.4.0/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('gentelella-1.4.0/vendors/nprogress/nprogress.css') }}" rel="stylesheet">

    <!-- Semantic -->
    <link href="{{ asset('semantic/dist/semantic.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/site.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/reset.css') }}" rel="stylesheet">

    <link href="{{ asset('semantic/dist/components/card.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/button.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/step.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/icon.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/grid.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/tab.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/segment.css') }}" rel="stylesheet">
    <link href="{{ asset('semantic/dist/components/dimmer.css') }}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="{{ asset('gentelella-1.4.0/build/css/custom.min.css') }}" rel="stylesheet">

    <!-- jQuery -->
    <script src="{{ asset('gentelella-1.4.0/vendors/jquery/dist/jquery.min.js') }}"></script>



    <!-- Semanticui -->
    <script src="{{ asset('semantic/dist/semantic.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/site.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/api.min.js') }}"></script>
    <script src="{{ asset('semantic/state.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/tab.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/dimmer.min.js') }}"></script>

</head>

<body class="nav-sm">
<div class="container body">
    <div class="main_container">
        <!-- Side Bar Navigation -->
        @include('voters.templates.sidebar')
        <!-- Top Navigation Bar -->
        @include('voters.templates.navbar')
        <!-- Body Content -->
        @yield('content')
        <!-- Footer -->
        @include('voters.templates.footer')
    </div>
</div>


<!-- Voters API -->
<script src="{{ asset('js/api.voters.js') }}"></script>


<!-- Bootstrap -->
<script src="{{ asset('gentelella-1.4.0/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('gentelella-1.4.0/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('gentelella-1.4.0/vendors/nprogress/nprogress.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('gentelella-1.4.0/build/js/custom.min.js') }}"></script>
</body>
</html>


