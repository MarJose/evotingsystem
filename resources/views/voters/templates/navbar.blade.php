<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            {{--<div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>--}}

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ asset(auth()->guard('student')->user()->avatar) }}" alt="Student Profile Picture">
                        {{ auth()->guard('student')->user()->FirstName }} {{ auth()->guard('student')->user()->LastName }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="#"> Profile</a></li>
                        <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit(); "><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                            <form id="logout-form" action="{{ route('voters.logout') }}" method="POST" style="display: none;">
                                @csrf
                                @method('POST')
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
