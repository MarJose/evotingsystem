@extends('voters.templates.master_template')


@section('title', 'eVote System')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">

        <div class="ui two column grid" style="margin-left: 5px;">
            <div class="row">
                <div class="ui ordered stackable top attached steps tabular menu">
                    <div class="column active">
                        <a class="step" data-tab="president">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">President</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step" data-tab="vpresident">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Vice President</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Secretary</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Treasurer</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Auditor</div>
                                <div class="description">Vote for Two</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Public Information Officer</div>
                                <div class="description">Vote for Two</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Business Manager</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Senior Representative</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a class="disabled step">
                            <div class="content" style="margin-top: 6px;">
                                <div class="title">Junior Representative</div>
                                <div class="description">Vote for One</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <div class="ui attached active tab segment" data-tab="president">

            <div class="ui three stackable grid">
                <div class="column">
                    <div class="ui special cards">
                        {{-- Template--}}
                        @foreach($presidents as $president)
                            <div class="card">
                                <div class="blurring dimmable image" id="president-card" >
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div class="ui inverted green button" data-id="{{ $president->id }}" data-position="{{ $president->Position }}">
                                                    <i class="thumbs up outline icon"></i> <strong>Vote</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="{{ asset($president->StudentPicture) }}" alt="Candidate Image">
                                </div>
                                <div class="content">
                                    <a class="header">
                                        {{ $president->FName }} {{$president->LName}}
                                    </a>
                                    <div class="meta">
                                        <span class="date">Grade {{$president->Gradelvl}}</span>
                                    </div>
                                </div>
                                <div class="extra content">
                                    <a>
                                        <i class="users icon"></i>
                                        {{ $president->organizations[0]['Organization_name'] }}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="vpresident">
            vpresident
        </div>




    </div>
    <!-- /page content -->
    <script>
        $('.menu .step')
            .tab();

        // Candidates Card
        $('.special.cards .image').dimmer({
            on: 'hover'
        });
    </script>
@endsection
