@if(session()->has('alert-success'))
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <i class="fa fa-check fa fa-2x"></i>
        <strong> Success!  </strong>
        {{ session('alert-success') }}
    </div>

@endif

@if(session()->has('alert-warning'))
    <div class="alert alert-warning alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <i class="fa fa-exclamation-triangle fa fa-2x"></i>
        <strong> Warning!  </strong>
        {{ session('alert-warning') }}
    </div>
@endif

@if(session()->has('alert-danger'))
       <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <i class="fa fa-exclamation-triangle fa fa-2x"></i>
        <strong> Alert!  </strong>
        {{ session('alert-danger') }}
    </div>
@endif

@if(session()->has('alert-info'))
    <div class="alert alert-info alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <i class="fa fa-info-circle fa fa-2x"></i>
        <strong> Info!  </strong>
        {{ session('alert-info') }}
    </div>
@endif
