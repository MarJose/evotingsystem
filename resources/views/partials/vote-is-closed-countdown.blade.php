<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/reset.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/site.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/semantic.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/container.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/grid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/image.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/label.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/divider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/segment.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/form.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/input.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/button.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/list.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/message.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/icon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/modal.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/transition.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('semantic/dist/components/dimmer.css') }}">

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/form.js') }}"></script>
    <script src="{{ asset('semantic/dist/semantic.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/modal.js') }}"></script>
    <script src="{{ asset('semantic/dist/components/transition.js') }}"></script>



    <script>
        $(document).ready(function(){
            $('#voteCountDown')
                .modal('show')
                .modal('setting', 'closable', false);
        });
    </script>
<body>
</body>
</head>
</html>

<div class="ui basic modal" id="voteCountDown">
    <div class="ui icon header">
        <i id="iconhour" class="ui green huge inverted hourglass outline icon loading"></i>
        <div id="timedisplayhere"><i class="notched circle loading icon "></i> Loading time...</div>
        <div class="ui massive red label" id="message" style="margin-top: 10px;">
            Voting System is temporary Closed..
        </div>
    </div>
    <div class="content">
        <p class="ui two column centered grid" id="contentmessage"> <strong> Voting System is temporary Closed. Voting System will open after the Count down is done. </strong>
    </div>
</div>
<script>

    var countDownDate = new Date(" {{ date('M j, Y', strtotime( session()->get('datestart')) )  }}, {{ session()->get('timestart') }}").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("timedisplayhere").innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById('message').classList.add('green');
            document.getElementById('timedisplayhere').style.display = 'none';
            document.getElementById('iconhour').classList.remove('loading');
            document.getElementById("message").innerHTML = "Voting System is now Open.";
            document.getElementById("contentmessage").innerHTML = "Please Refresh the page.";
        }
    }, 1000);
</script>

